package io.chino.java;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.common.*;
import io.chino.api.common.DumpFormat;
import io.chino.api.schema.SchemaDumpFiltering;
import io.chino.api.schema.SchemaDumpIdWrapper;
import io.chino.api.schema.SchemaDumpStatus;
import io.chino.api.search.SearchQueryBuilder;
import io.chino.api.search.SortRule;
import io.chino.api.userschema.*;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Manage the {@link UserSchema UserSchemas} that define attributes stored for your {@link io.chino.api.user.User Users}.
 */
public class UserSchemas extends ChinoBaseAPI {

    private final ChinoAPI parent;
    private final String hostUrl;

    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl      the base URL of the Chino.io API. For testing, use:<br>
     *                        {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     */
    public UserSchemas(String baseApiUrl, ChinoAPI parentApiClient) {
        super(baseApiUrl, parentApiClient);
        hostUrl = baseApiUrl;
        parent = parentApiClient;
    }

    /**
     * List all the {@link UserSchema UserSchemas} in the account
     *
     * @param offset page offset of the results.
     * @param limit the max amount of results to be returned.
     *
     * @return a {@link GetUserSchemasResponse} that wraps a {@link List} of {@link UserSchema UserSchemas}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetUserSchemasResponse list(int offset, int limit) throws IOException, ChinoApiException {
        JsonNode data = getResource("/user_schemas", offset, limit);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemasResponse.class);
        return null;
    }

    /**
     * List all the {@link UserSchema UserSchemas} in the account
     *
     * @return a {@link GetUserSchemasResponse} that wraps a {@link List} of {@link UserSchema UserSchemas}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetUserSchemasResponse list() throws IOException, ChinoApiException {
        JsonNode data = getResource("/user_schemas", 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemasResponse.class);
        return null;
    }

    /**
     * List all the {@link UserSchema UserSchemas} that <b>contain</b>
     * the provided {@link String} in the description. The comparison is case-sensitive.
     *
     * @param filterByDescription a String to perform the filtering. The API will return {@link UserSchema UserSchemas}
     *                            that contain this String in their description (case-sensitive)
     *
     * @return a {@link GetUserSchemasResponse} that wraps a {@link List} of {@link UserSchema UserSchemas} that contain
     *         the String in the description
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetUserSchemasResponse list(String filterByDescription) throws IOException, ChinoApiException {
        return this.list(filterByDescription, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
    }

    /**
     * List all the {@link UserSchema UserSchemas} that <b>contain</b>
     * the provided {@link String} in the description. The comparison is case-sensitive.
     *
     * @param filterByDescription a String to perform the filtering. The API will return {@link UserSchema UserSchemas}
     *                            that contain this String in their description (case-sensitive)
     * @param offset page offset of the results.
     * @param limit the max amount of results to be returned
     *
     * @return a {@link GetUserSchemasResponse} that wraps a {@link List} of {@link UserSchema UserSchemas} that
     * contain the String in the description
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetUserSchemasResponse list(String filterByDescription, int offset, int limit) throws IOException, ChinoApiException {
        HashMap<String, String> params = new HashMap<>();
        if (filterByDescription != null) {
            params.put("descr", filterByDescription);
        }
        params.put("offset", String.valueOf(offset));
        params.put("limit", String.valueOf(limit));
        JsonNode data = getResource("/user_schemas", params);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemasResponse.class);
        return null;
    }

    /**
     * Read a specific {@link UserSchema}
     *
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     *
     * @return the requested {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema read(String userSchemaId) throws IOException, ChinoApiException{
        checkNotNull(userSchemaId, "user_schema_id");
        JsonNode data = getResource("/user_schemas/"+userSchemaId, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemaResponse.class).getUserSchema();
        return null;
    }

    /**
     * Create a new {@link UserSchema} on Chino.io
     *
     * @param description a description of the new {@link UserSchema}
     * @param userSchemaStructure a {@link UserSchemaStructure} object which describes the structure of the new UserSchema.
     *
     * @return the new {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema create(String description, UserSchemaStructure userSchemaStructure) throws IOException, ChinoApiException {
        checkNotNull(userSchemaStructure, "user_schema_structure");
        UserSchemaRequest userSchemaRequest= new UserSchemaRequest(description, userSchemaStructure);

        JsonNode data = postResource("/user_schemas", userSchemaRequest);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemaResponse.class).getUserSchema();

        return null;
    }

    /**
     * Create a new {@link UserSchema} on Chino.io
     *
     * @param userSchemaRequest a {@link UserSchemaRequest} Object which contains a description and the
     *                          UserSchema's fields
     *
     * @return the new {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema create(UserSchemaRequest userSchemaRequest) throws IOException, ChinoApiException {
        checkNotNull(userSchemaRequest, "user_schema_request");
        JsonNode data = postResource("/user_schemas", userSchemaRequest);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemaResponse.class).getUserSchema();

        return null;
    }

    /**
     * Create a new {@link UserSchema} on Chino.io. The attributes of the schema are inferred from the fields
     * of class "myClass".
     *
     * @param description a description of the new {@link UserSchema}
     * @param myClass a Java {@link Class} that represents the structure of the new UserSchema.
     *                Mark fields that need to be indexed with the annotation
     *                {@link io.chino.api.common.indexed @indexed}.
     *
     * @return the new {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
     public UserSchema create(String description, Class myClass) throws IOException, ChinoApiException {
        List<Field> fieldsList = returnFields(myClass);
        UserSchemaStructure userSchemaStructure = new UserSchemaStructure(fieldsList);
        UserSchemaRequest schemaRequest= new UserSchemaRequest(description, userSchemaStructure);

        JsonNode data = postResource("/user_schemas", schemaRequest);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemaResponse.class).getUserSchema();

        return null;
    }

    /**
     * Update the specified {@link UserSchema}
     *
     * @param activateResource if true, the update method will set {@code "is_active": true} in the resource.
     *                         Otherwise, the value of is_active will not be modified.
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     * @param userSchemaRequest a {@link UserSchemaRequest} Object which contains the new description and the
     *                          new structure of the UserSchema's fields
     *
     * @return the updated {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema update(boolean activateResource, String userSchemaId, UserSchemaRequest userSchemaRequest) throws IOException, ChinoApiException {
        checkNotNull(userSchemaId, "user_schema_id");
        checkNotNull(userSchemaRequest, "user_schema_request");
        if (activateResource)
            userSchemaRequest.activateResource();
        JsonNode data = putResource("/user_schemas/"+userSchemaId, userSchemaRequest);
        if(data!=null)
            return getMapper().convertValue(data, GetUserSchemaResponse.class).getUserSchema();
        return null;
    }

    /**
     * Update the specified {@link UserSchema}
     *
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     * @param userSchemaRequest a {@link UserSchemaRequest} Object which contains the new description and the
     *                          new structure of the UserSchema's fields
     *
     * @return the updated {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema update(String userSchemaId, UserSchemaRequest userSchemaRequest) throws IOException, ChinoApiException {
        return update(false, userSchemaId, userSchemaRequest);
    }

    /**
     * Update the specified {@link UserSchema}
     *
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     * @param description the new description for the UserSchema
     * @param userSchemaStructure a {@link UserSchemaStructure} object which describes the new structure
     *                            of the UserSchema.
     *
     * @return the updated {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema update(String userSchemaId, String description, UserSchemaStructure userSchemaStructure) throws IOException, ChinoApiException {
        return update(false, userSchemaId, new UserSchemaRequest(description, userSchemaStructure));
    }

    /**
     * Update the specified {@link UserSchema}
     *
     * @param activateResource if true, the update method will set {@code "is_active": true} in the resource.
     *                         Otherwise, the value of is_active will not be modified.
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     * @param description the new description for the UserSchema
     * @param userSchemaStructure a {@link UserSchemaStructure} object which describes the new structure
     *                            of the UserSchema.
     *
     * @return the updated {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema update(boolean activateResource, String userSchemaId, String description, UserSchemaStructure userSchemaStructure) throws IOException, ChinoApiException {
        checkNotNull(userSchemaId, "user_schema_id");
        checkNotNull(userSchemaStructure, "user_schema_structure");

        return update(activateResource, userSchemaId, new UserSchemaRequest(description, userSchemaStructure));
    }

    /**
     * Update the specified {@link UserSchema}
     *
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     * @param description the new description of the {@link UserSchema}
     * @param myClass a Java {@link Class} that represents the new structure of the UserSchema.
     *                Mark fields that need to be indexed with the annotation
     *                {@link io.chino.api.common.indexed @indexed}.
     *
     * @return the updated {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema update(String userSchemaId, String description, Class myClass) throws IOException, ChinoApiException {
        return update(false, userSchemaId, description, myClass);
    }

    /**
     * Update the specified {@link UserSchema}
     *
     * @param activateResource if true, the update method will set {@code "is_active": true} in the resource.
     *                         Otherwise, the value of is_active will not be modified.
     * @param userSchemaId the id of the {@link UserSchema} on Chino.io
     * @param description the new description of the {@link UserSchema}
     * @param myClass a Java {@link Class} that represents the new structure of the UserSchema.
     *                Mark fields that need to be indexed with the annotation
     *                {@link io.chino.api.common.indexed @indexed}.
     *
     * @return the updated {@link UserSchema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public UserSchema update(boolean activateResource, String userSchemaId, String description, Class myClass) throws IOException, ChinoApiException {
        checkNotNull(userSchemaId, "user_schema_id");
        List<Field> fieldsList = returnFields(myClass);
        UserSchemaStructure userSchemaStructure = new UserSchemaStructure(fieldsList);
        UserSchemaRequest userSchemaRequest= new UserSchemaRequest(description, userSchemaStructure);

        return update(activateResource, userSchemaId, userSchemaRequest);
    }

    /**
     * Delete a {@link UserSchema} from Chino.io
     *
     * @param userSchemaId the id of the {@link UserSchema}
     * @param force if {@code true}, the resource will be deleted forever. Otherwise, it will only be deactivated.
     *
     * @return a String with the result of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String delete(String userSchemaId, boolean force) throws IOException, ChinoApiException {
        checkNotNull(userSchemaId, "user_schema_id");
        return deleteResource("/user_schemas/"+userSchemaId, force);
    }


    /**
     * Start an operation of Datatype Conversion in a {@link UserSchema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link UserSchema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#user-schemas-userschema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param userSchemaId ID of the target {@link UserSchema}
     * @param fields a {@link HashMap} that maps field names to their new datatype
     * @param defaultValues a {@link HashMap} that maps field names to the new default values (optional)
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String userSchemaId, @NotNull HashMap<String, String> fields, HashMap<String, Object> defaultValues)
            throws IOException, ChinoApiException
    {
        checkNotNull(fields, "fields");

        HashMap<String, HashMap> body = new HashMap<>();
        body.put("fields", fields);
        if (defaultValues != null) {
            body.put("defaults", defaultValues);
        }

        JsonNode data = postResource("/user_schemas/convert/" + userSchemaId, body);
        if (data != null)
            return mapper.convertValue(data, DatatypeConversionStarted.class);

        return null;
    }

    /**
     * Start an operation of Datatype Conversion in a {@link UserSchema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link UserSchema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#user-schemas-userschema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param userSchemaId ID of the target {@link UserSchema}
     * @param fields a {@link HashMap} that maps field names to their new datatype
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String userSchemaId, HashMap<String, String> fields) throws IOException, ChinoApiException {
        return this.startDatatypeConversion(userSchemaId, fields, null);
    }

    /**
     * Start an operation of Datatype Conversion in a {@link UserSchema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link UserSchema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#user-schemas-userschema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param userSchemaId ID of the target {@link UserSchema}
     * @param fields a varargs array of {@link Field Fields} containing the new datatype.
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String userSchemaId, Field ... fields) throws IOException, ChinoApiException {
        // extract values from fields
        HashMap<String, String> fieldsMap = new HashMap<>();
        HashMap<String, Object> defaultValues = new HashMap<>();
        for (Field field : fields) {
            String name = field.getName();
            String newType = field.getType();
            Object defaultValue = field.getDefault();

            fieldsMap.put(name, newType);

            if (defaultValue != Field.Default.NOT_SET) {
                defaultValues.put(name, defaultValue);
            }
        }
        // start the actual conversion
        if (defaultValues.isEmpty()) {
            return this.startDatatypeConversion(userSchemaId, fieldsMap);
        } else {
            return this.startDatatypeConversion(userSchemaId, fieldsMap, defaultValues);
        }
    }

    /**
     * Start an operation of Datatype Conversion in a {@link UserSchema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link UserSchema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#user-schemas-userschema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param userSchemaId ID of the target {@link UserSchema}
     * @param fields a {@link Collection} containing {@link Field Fields} with the new datatype
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String userSchemaId, Collection<Field> fields) throws IOException, ChinoApiException {
        Field[] fieldsArray = new Field[0];
        fieldsArray = fields.toArray(fieldsArray);
        return this.startDatatypeConversion(userSchemaId, fieldsArray);
    }

    /**
     * Read the status of a Datatype Conversion operation on a {@link UserSchema}
     *
     * @param userSchemaId the ID of the targeted {@link UserSchema}
     *
     * @return an instance of {@link DatatypeConversionStatus}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStatus getDatatypeConversionStatus(@NotNull String userSchemaId) throws IOException, ChinoApiException {
        checkNotNull(userSchemaId, "user_schema_id");

        JsonNode data = getResource("/user_schemas/convert/" + userSchemaId + "/status");
        if (data != null)
            return mapper.convertValue(data, DatatypeConversionStatus.class);

        return null;
    }

    /**
     * Trigger a UserSchema Dump operation.<br>
     * Schema Dump is an asynchronous task. Use {@link #checkUserSchemaDumpStatus(String)} to check the task progress and
     * {@link #getUserSchemaDump(String, String, String)} to get the result when completed.
     *
     * @param userSchemaId ID of the Schema to dump
     * @param fields list of fields to include in the dump file
     * @param query {@link SearchQueryBuilder} used to filter the documents to include in the dump file
     * @param sort list of {@link SortRule} to affect the order of the entries in the dump file. Can be {@code null}.
     * @param format format of the output file. Can be {@code null}, in which case {@link #JSON JSON} is used.
     *
     * @return the {@code dump_id} as a String. The value is needed to check the task progress and download the dump.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String startUserSchemaDump(String userSchemaId, List<String> fields, SearchQueryBuilder query, List<SortRule> sort, DumpFormat format) throws IOException, ChinoApiException {
        checkNotNull(userSchemaId, "user_schema_id");

        SchemaDumpFiltering filtering = new SchemaDumpFiltering(fields, query, sort, format);
        JsonNode data = postResource("/user_schemas/dump/" + userSchemaId, filtering.toHashMap());

        if (data != null) {
            SchemaDumpIdWrapper responseWrapper = mapper.convertValue(data, SchemaDumpIdWrapper.class);
            return responseWrapper.getDumpId();
        }

        return null;
    }

    /**
     * Check the status of the UserSchema Dump task
     *
     * @param dumpId the ID returned by {@link #startUserSchemaDump(String, List, SearchQueryBuilder, List, DumpFormat)}
     *
     * @return the {@link SchemaDumpStatus} with information about the task progress.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public SchemaDumpStatus checkUserSchemaDumpStatus(String dumpId) throws IOException, ChinoApiException {
        checkNotNull(dumpId, "dump_id");

        JsonNode data = getResource("/user_schemas/dump/" + dumpId + "/status");

        if (data != null) {
            return mapper.convertValue(data, SchemaDumpStatus.class);
        }
        return null;
    }

    /**
     * Download the dump as file
     *
     * @param dumpId the ID returned by {@link #startUserSchemaDump(String, List, SearchQueryBuilder, List, DumpFormat)}
     * @param destination path to the folder where the dump file must be saved
     * @param fileName name to assign to the dump file. Can be {@code null}, in which case a default name will be
     *                 assigned, containing the {@code dumpId}.
     *
     * @return a {@link File} object pointing to the downloaded dump file
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public File getUserSchemaDump(String dumpId, String destination, String fileName) throws IOException, ChinoApiException {
        checkNotNull(dumpId, "dump_id");
        checkNotNull(destination, "destination");

        String downloadUrl = hostUrl + "/user_schemas/dump/" + dumpId + "/download";
        Request request = new Request.Builder().url(downloadUrl).get().build();
        Response response = parent.getHttpClient().newCall(request).execute();

        if (response.code() != 200) {
            ErrorResponse error = new ObjectMapper().readValue(
                    response.body().byteStream(),
                    ErrorResponse.class);
            throw new ChinoApiException(error);
        }

        if (fileName == null) {
            fileName = "chino_io_dump_" + dumpId + ".txt";
        }

        return DumpUtils.writeDump(response, destination, fileName);
    }

}
