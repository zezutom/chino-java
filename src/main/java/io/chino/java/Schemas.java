package io.chino.java;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.common.*;
import io.chino.api.common.DumpFormat;
import io.chino.api.schema.*;
import io.chino.api.schema.SchemaDumpIdWrapper;
import io.chino.api.search.SearchQueryBuilder;
import io.chino.api.search.SortRule;
import okhttp3.Request;
import okhttp3.Response;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * Manage the {@link Schema Schemas} that describe the structure of the fields in your Documents.
 */
public class Schemas extends ChinoBaseAPI {

    private final ChinoAPI parent;
    private final String hostUrl;

    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl      the base URL of the Chino.io API. For testing, use:<br>
     *                        {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     */
    public Schemas(String baseApiUrl, ChinoAPI parentApiClient) {
        super(baseApiUrl, parentApiClient);
        hostUrl = baseApiUrl;
        parent = parentApiClient;
    }

    /**
     * List all the {@link Schema Schemas} inside a {@link io.chino.api.repository.Repository Repository}.
     *
     * @param repositoryId the ID of the {@link io.chino.api.repository.Repository Repository}
     * @param offset page offset of the results.
     * @param limit the max amount of results to be returned.
     *
     * @return A {@link GetSchemasResponse} that wraps a list of {@link Schema Schemas}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetSchemasResponse list(String repositoryId, int offset, int limit) throws IOException, ChinoApiException {
        checkNotNull(repositoryId, "repository_id");
        JsonNode data = getResource("/repositories/"+repositoryId+"/schemas", offset, limit);
        if(data!=null)
            return mapper.convertValue(data, GetSchemasResponse.class);
        return null;
    }

    /**
     * List all the {@link Schema Schemas} inside a {@link io.chino.api.repository.Repository Repository}.
     *
     * @param repositoryId the ID of the {@link io.chino.api.repository.Repository Repository}
     *
     * @return A {@link GetSchemasResponse} that wraps a list of {@link Schema Schemas}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetSchemasResponse list(String repositoryId) throws IOException, ChinoApiException {
        checkNotNull(repositoryId, "repository_id");
        JsonNode data = getResource("/repositories/"+repositoryId+"/schemas",
                0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return mapper.convertValue(data, GetSchemasResponse.class);
        return null;
    }

    /**
     * Filter the {@link Schema Schemas} of a {@link io.chino.api.repository.Repository Repository} that <b>contain</b>
     * the provided {@link String} in the description. The comparison is case-sensitive.
     *
     * @param repositoryId the ID of the {@link io.chino.api.repository.Repository Repository}
     * @param filterByDescription a String to perform the filtering. The API will return {@link Schema Schemas} that
     *                            contain this String in their description (case-sensitive)
     * @param offset page offset of the results.
     * @param limit the max amount of results to be returned.
     *
     * @return A {@link GetSchemasResponse} that wraps a list of {@link Schema Schemas} that contain the String in the
     *         description
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetSchemasResponse list(String repositoryId, String filterByDescription, int offset, int limit)
            throws IOException, ChinoApiException
    {
        checkNotNull(repositoryId, "repository_id");
        HashMap<String, String> params = new HashMap<>();
        if (filterByDescription != null)
            params.put("descr", filterByDescription);
        params.put("offset", String.valueOf(offset));
        params.put("limit", String.valueOf(limit));
        JsonNode data = getResource("/repositories/" + repositoryId + "/schemas", params);
        if(data!=null)
            return mapper.convertValue(data, GetSchemasResponse.class);
        return null;
    }

    /**
     * Filter the {@link Schema Schemas} of a {@link io.chino.api.repository.Repository Repository} that <b>contain</b>
     * the provided {@link String} in the description. The comparison is case-sensitive.
     *
     * @param repositoryId the ID of the {@link io.chino.api.repository.Repository Repository}
     * @param filterByDescription a String to perform the filtering. The API will return {@link Schema Schemas} that
     *                            contain this String in their description (case-sensitive)
     *
     * @return A {@link GetSchemasResponse} that wraps a list of {@link Schema Schemas} that contain the String in the
     *         description
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetSchemasResponse list(String repositoryId, String filterByDescription)
            throws IOException, ChinoApiException
    {
        return this.list(repositoryId, filterByDescription, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
    }

    /**
     * Read a specific Schema from Chino.io
     *
     * @param schemaId the ID of the {@link Schema}
     *
     * @return the requested {@link Schema}
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema read(String schemaId) throws IOException, ChinoApiException{
        checkNotNull(schemaId, "schema_id");
        JsonNode data = getResource("/schemas/"+schemaId, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return mapper.convertValue(data, GetSchemaResponse.class).getSchema();

        return null;
    }

    /**
     * Create a new {@link Schema} on Chino.io
     *
     * @param repositoryId the id of the {@link io.chino.api.repository.Repository Repository}
     * @param schemaRequest a {@link SchemaRequest} which contains the new {@link Schema}'s structure
     *
     * @return the new {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema create(String repositoryId, SchemaRequest schemaRequest) throws IOException, ChinoApiException {
        checkNotNull(repositoryId, "repository_id");
        checkNotNull(schemaRequest, "schema_request");
        JsonNode data = postResource("/repositories/"+repositoryId+"/schemas", schemaRequest);
        if(data!=null)
            return mapper.convertValue(data, GetSchemaResponse.class).getSchema();

        return null;
    }

    /**
     * Create a new {@link Schema} on Chino.io, based on the fields of a Java class
     *
     * @param repositoryId the ID of the {@link io.chino.api.repository.Repository}
     * @param description the description of the {@link Schema}
     * @param myClass a {@link Class} that represents the structure of the new {@link Schema}
     *
     * @return the new {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see io.chino.api.common.indexed @indexed
     */
    public Schema create(String repositoryId, String description, Class myClass) throws IOException, ChinoApiException {
        checkNotNull(repositoryId, "repository_id");
        SchemaRequest schemaRequest = new SchemaRequest();
        schemaRequest.setDescription(description);
        SchemaStructure schemaStructure = new SchemaStructure();
        List<Field> fieldsList = returnFields(myClass);
        schemaStructure.setFields(fieldsList);
        schemaRequest.setStructure(schemaStructure);

        JsonNode data = postResource("/repositories/"+repositoryId+"/schemas", schemaRequest);
        if(data!=null)
            return mapper.convertValue(data, GetSchemaResponse.class).getSchema();

        return null;
    }

    /**
     * Create a new {@link Schema} on Chino.io
     *
     * @param repositoryId the id of the {@link io.chino.api.repository.Repository Repository}
     * @param schemaStructure a {@link SchemaStructure} which contains the new {@link Schema}'s structure
     *
     * @return the new {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema create(String repositoryId, String description, SchemaStructure schemaStructure) throws IOException, ChinoApiException {
        checkNotNull(repositoryId, "repository_id");
        SchemaRequest schemaRequest= new SchemaRequest(description, schemaStructure);

        return create(repositoryId, schemaRequest);
    }

    /**
     * Update an existing {@link Schema}
     *
     * @param schemaId the ID of the Schema to update
     * @param schemaRequest a {@link SchemaStructure} object that contains the new description
     *                      and structure of the Schema
     *
     * @return the updated {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema update(String schemaId, SchemaRequest schemaRequest) throws IOException, ChinoApiException {
        return update(false, schemaId, schemaRequest);
    }

    /**
     * Update an existing {@link Schema}
     *
     * @param schemaId the ID of the Schema to update
     * @param schemaRequest a {@link SchemaStructure} object that contains the new description
     *                      and structure of the Schema
     *
     * @return the updated {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema update(boolean activateResource, String schemaId, SchemaRequest schemaRequest) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");
        if (activateResource) {
            schemaRequest.activateResource();
        }
        JsonNode data = putResource("/schemas/" + schemaId, schemaRequest);
        if (data != null)
            return mapper.convertValue(data, GetSchemaResponse.class).getSchema();

        return null;
    }

    /**
     * Update an existing {@link Schema}
     *
     * @param schemaId the ID of the Schema to update
     * @param description the new description of the Schema
     * @param schemaStructure a {@link SchemaStructure} object that contains the new structure of the Schema
     *
     * @return the updated {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema update(String schemaId, String description, SchemaStructure schemaStructure) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");
        SchemaRequest schemaRequest = new SchemaRequest(description, schemaStructure);
        return update(false, schemaId, schemaRequest);
    }

    /**
     * Update an existing {@link Schema}
     *
     * @param schemaId the ID of the Schema to update
     * @param description the new description of the Schema
     * @param schemaStructure a {@link SchemaStructure} object that contains the new structure of the Schema
     *
     * @return the updated {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Schema update(boolean activateResource, String schemaId, String description, SchemaStructure schemaStructure) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");
        SchemaRequest schemaRequest = new SchemaRequest(description, schemaStructure);
        return update(activateResource, schemaId, schemaRequest);
    }

    /**
     * Update an existing {@link Schema}
     *
     * @param schemaId the ID of the Schema to update
     * @param description the new description of the Schema
     * @param myClass {@link Class} that represents the new structure of the {@link Schema}
     *
     * @return the updated {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see io.chino.api.common.indexed @indexed
     */
    public Schema update(String schemaId, String description, Class myClass) throws IOException, ChinoApiException {
        return update(false, schemaId, description, myClass);
    }

    /**
     * Update an existing {@link Schema}
     *
     * @param schemaId the ID of the Schema to update
     * @param description the new description of the Schema
     * @param myClass {@link Class} that represents the new structure of the {@link Schema}
     *
     * @return the updated {@link Schema}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see io.chino.api.common.indexed @indexed
     */
    public Schema update(boolean activateResource, String schemaId, String description, Class myClass) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");
        SchemaRequest schemaRequest = new SchemaRequest();
        schemaRequest.setDescription(description);
        SchemaStructure schemaStructure = new SchemaStructure();
        List<Field> fieldsList = returnFields(myClass);
        schemaStructure.setFields(fieldsList);
        schemaRequest.setStructure(schemaStructure);

        return update(activateResource, schemaId, schemaRequest);
    }

    /**
     * Delete a {@link Schema} from Chino.io
     *
     * @param schemaId the ID of the {@link Schema} on Chino.io
     * @param force if true, the resource cannot be restored. Otherwise, it will only be deactivated.
     *
     * @return a String with the result of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String delete(String schemaId, boolean force) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");
        return deleteResource("/schemas/"+schemaId, force);
    }
    
    /**
     * Delete a {@link Schema} from Chino.io
     *
     * @param schemaId the ID of the {@link Schema} on Chino.io
     * @param force if true, the resource cannot be restored. Otherwise, it will only be deactivated.
     * @param deleteAllContent if true, delete the Schema even if not empty. All the content will be deleted as well (cascade).
     *
     * @return a String with the result of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String delete(String schemaId, boolean force, boolean deleteAllContent) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");
        HashMap<String, String> params = new HashMap<>();
        params.put("force", force ? "true" : "false");
        params.put("all_content", deleteAllContent ? "true" : "false");
        return deleteResource("/schemas/"+schemaId, params);
    }

    /**
     * Start an operation of Datatype Conversion in a {@link Schema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link Schema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#schemas-schema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param schemaId ID of the target {@link Schema}
     * @param fields a {@link HashMap} that maps field names to their new datatype
     * @param defaultValues a {@link HashMap} that maps field names to the new default values (optional)
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String schemaId, @NotNull HashMap<String, String> fields, HashMap<String, Object> defaultValues)
            throws IOException, ChinoApiException
    {
        checkNotNull(fields, "fields");

        HashMap<String, HashMap> body = new HashMap<>();
        body.put("fields", fields);
        if (defaultValues != null) {
            body.put("defaults", defaultValues);
        }

        JsonNode data = postResource("/schemas/convert/" + schemaId, body);
        if (data != null)
            return mapper.convertValue(data, DatatypeConversionStarted.class);

        return null;
    }

    /**
     * Start an operation of Datatype Conversion in a {@link Schema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link Schema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#schemas-schema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param schemaId ID of the target {@link Schema}
     * @param fields a {@link HashMap} that maps field names to their new datatype
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String schemaId, HashMap<String, String> fields) throws IOException, ChinoApiException {
        return this.startDatatypeConversion(schemaId, fields, null);
    }

    /**
     * Start an operation of Datatype Conversion in a {@link Schema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link Schema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#schemas-schema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param schemaId ID of the target {@link Schema}
     * @param fields a varargs array of {@link Field Fields} containing the new datatype.
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String schemaId, Field... fields) throws IOException, ChinoApiException {
        // extract values from fields
        HashMap<String, String> fieldsMap = new HashMap<>();
        HashMap<String, Object> defaultValues = new HashMap<>();
        for (Field field : fields) {
            String name = field.getName();
            String newType = field.getType();
            Object defaultValue = field.getDefault();

            fieldsMap.put(name, newType);

            if (defaultValue != Field.Default.NOT_SET) {
                defaultValues.put(name, defaultValue);
            }
        }
        // start the actual conversion
        if (defaultValues.isEmpty()) {
            return this.startDatatypeConversion(schemaId, fieldsMap);
        } else {
            return this.startDatatypeConversion(schemaId, fieldsMap, defaultValues);
        }
    }

    /**
     * Start an operation of Datatype Conversion in a {@link Schema}.
     *
     * The operation is async, it may take up to a few minutes and the {@link Schema} will be locked for the entire
     * duration of the operation. <br>
     * <br>
     * <h3>WARNING: Datatype Conversion may cause loss of data.</h3> Be sure you
     * <a href="https://docs.chino.io/custodia/docs/v1#schemas-schema-datatype-conversion">
     *     read the documentation
     * </a> and understand the consequences before starting Datatype Conversion on a Schema.
     *
     * @param schemaId ID of the target {@link Schema}
     * @param fields a {@link Collection} containing {@link Field Fields} with the new datatype
     *
     * @return {@link DatatypeConversionStarted} containing any message returned by the Chino.io API
     *
     * @see #getDatatypeConversionStatus(String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStarted startDatatypeConversion(String schemaId, Collection<Field> fields) throws IOException, ChinoApiException {
        Field[] fieldsArray = new Field[0];
        fieldsArray = fields.toArray(fieldsArray);
        return this.startDatatypeConversion(schemaId, fieldsArray);
    }

    /**
     * Read the status of a Datatype Conversion operation on a {@link Schema}
     *
     * @param schemaId the ID of the targeted {@link Schema}
     *
     * @return an instance of {@link DatatypeConversionStatus}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public DatatypeConversionStatus getDatatypeConversionStatus(@NotNull String schemaId) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");

        JsonNode data = getResource("/schemas/convert/" + schemaId + "/status");
        if (data != null)
            return mapper.convertValue(data, DatatypeConversionStatus.class);

        return null;
    }

    /**
     * Trigger a Schema Dump operation.<br>
     * Schema Dump is an asynchronous task. Use {@link #checkSchemaDumpStatus(String)} to check the task progress and
     * {@link #getSchemaDump(String, String, String)} to get the result when completed.
     *
     * @param schemaId ID of the Schema to dump
     * @param fields list of fields to include in the dump file
     * @param query {@link SearchQueryBuilder} used to filter the documents to include in the dump file
     * @param sort list of {@link SortRule} to affect the order of the entries in the dump file. Can be {@code null}.
     * @param format format of the output file. Can be {@code null}, in which case {@link #JSON JSON} is used.
     *
     * @return the {@code dump_id} as a String. The value is needed to check the task progress and download the dump.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String startSchemaDump(String schemaId, List<String> fields, SearchQueryBuilder query, List<SortRule> sort, DumpFormat format) throws IOException, ChinoApiException {
        checkNotNull(schemaId, "schema_id");

        SchemaDumpFiltering filtering = new SchemaDumpFiltering(fields, query, sort, format);
        JsonNode data = postResource("/schemas/dump/" + schemaId, filtering.toHashMap());

        if (data != null) {
            SchemaDumpIdWrapper responseWrapper = mapper.convertValue(data, SchemaDumpIdWrapper.class);
            return responseWrapper.getDumpId();
        }

        return null;
    }

    /**
     * Trigger a Schema Dump operation.<br>
     * Schema Dump is an asynchronous task. Use {@link #checkSchemaDumpStatus(String)} to check the task progress and
     * {@link #getSchemaDump(String, String, String)} to get the result when completed.
     *
     * @param schemaId ID of the Schema to dump
     * @param fields list of fields to include in the dump file
     * @param query {@link SearchQueryBuilder} used to filter the documents to include in the dump file
     * @param sort list of {@link SortRule} to affect the order of the entries in the dump file. Can be {@code null}.
     * @param format format of the output file. Can be {@code null}, in which case {@link #JSON JSON} is used.
     *
     * @return the {@code dump_id} as a String. The value is needed to check the task progress and download the dump.
     *
     * @deprecated moved class DumpFormat to package io.chino.api.common - this method is kept for compatibility and
     *             will be removed in a future major version
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    @Deprecated
    public String startSchemaDump(String schemaId, List<String> fields, SearchQueryBuilder query, List<SortRule> sort,
                                  io.chino.api.schema.DumpFormat format) throws IOException, ChinoApiException
    {
        // use DumpFormat from io.chino.api.common
        if (format == io.chino.api.schema.DumpFormat.CSV)
            return startSchemaDump(schemaId, fields, query, sort, DumpFormat.CSV);
        else  // null or JSON
            return startSchemaDump(schemaId, fields, query, sort, DumpFormat.JSON);
    }

    /**
     * Check the status of the Schema Dump task
     *
     * @param dumpId the ID returned by {@link #startSchemaDump(String, List, SearchQueryBuilder, List, DumpFormat)}
     *
     * @return the {@link SchemaDumpStatus} with information about the task progress.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public SchemaDumpStatus checkSchemaDumpStatus(String dumpId) throws IOException, ChinoApiException {
        checkNotNull(dumpId, "dump_id");

        JsonNode data = getResource("/schemas/dump/" + dumpId + "/status");

        if (data != null) {
            return mapper.convertValue(data, SchemaDumpStatus.class);
        }
        return null;
    }

    /**
     * Download the dump as file
     *
     * @param dumpId the ID returned by {@link #startSchemaDump(String, List, SearchQueryBuilder, List, DumpFormat)}
     * @param destination path to the folder where the dump file must be saved
     * @param fileName name to assign to the dump file. Can be {@code null}, in which case a default name will be
     *                 assigned, containing the {@code dumpId}.
     *
     * @return a {@link File} object pointing to the downloaded dump file
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public File getSchemaDump(String dumpId, String destination, String fileName) throws IOException, ChinoApiException {
        checkNotNull(dumpId, "dump_id");
        checkNotNull(destination, "destination");

        String downloadUrl = hostUrl + "/schemas/dump/" + dumpId + "/download";
        Request request = new Request.Builder().url(downloadUrl).get().build();
        Response response = parent.getHttpClient().newCall(request).execute();

        if (response.code() != 200) {
            ErrorResponse error = new ObjectMapper().readValue(
                    response.body().byteStream(),
                    ErrorResponse.class);
            throw new ChinoApiException(error);
        }

        if (fileName == null) {
            fileName = "chino_io_dump_" + dumpId + ".txt";
        }

        return Schemas.writeDump(response, destination, fileName);
    }

    private static File writeDump(Response apiResponse, String folderPath, String fileName) throws IOException {
        checkNotNull(apiResponse, "apiResponse");
        checkNotNull(folderPath, "folderPath");
        checkNotNull(fileName, "fileName");

        try {
            // read bytes from response and write to file
            InputStream returnStream = apiResponse.body().byteStream();

            String outpuPath = folderPath + File.separator + fileName;
            File outputFile = new File(outpuPath);
            outputFile.getParentFile().mkdirs();
            Files.copy(returnStream, outputFile.toPath());

            return outputFile;
        } finally { // close response body to prevent warnings from OkHttp3
            apiResponse.body().close();
        }
    }

}
