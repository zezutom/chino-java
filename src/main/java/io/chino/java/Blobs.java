package io.chino.java;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.blob.*;
import io.chino.api.common.ChinoApiException;
import io.chino.api.common.ErrorResponse;
import io.chino.api.document.Document;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * Manage upload and download of binary files as {@link Blobs BLOBs}.
 */
// when changing methods of this class, run BlobsTest.testMultiChunks() once
public class Blobs extends ChinoBaseAPI {

    /**
     * the default chunk size used by {@link #uploadBlob(String, String, String, String)}
     */
    private static final int CHUNK_SIZE = 1024 * 1024;

    private final ChinoAPI parent;
    private final String hostUrl;

    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl      the base URL of the Chino.io API. For testing, use:<br>
     *                        {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     *
     */
    public Blobs(String baseApiUrl, ChinoAPI parentApiClient) {
        super(baseApiUrl, parentApiClient);
        hostUrl = baseApiUrl;
        parent = parentApiClient;
    }

    /**
     * Upload a local file to Chino.io as a BLOB. This method fully handles the upload of a BLOB.
     *
     * @param folderPath the path to the folder which contains the local file
     * @param documentId the ID of the Chino.io {@link Document} which contains this BLOB
     * @param fieldName the name of the field (of type "blob") that refers to this BLOB in the Document
     * @param fileName the name of the local file to upload
     *
     * @return A {@link CommitBlobUploadResponse} that wraps a {@link CommitBlobUploadResponseContent},
     * with information on the blob itself.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public CommitBlobUploadResponse uploadBlob(String folderPath, String documentId, String fieldName, String fileName)
            throws IOException, ChinoApiException
    {
        checkNotNull(folderPath, "path");

        // get upload ID for uploading chunks
        CreateBlobUploadResponse blobResponse = initUpload(documentId, fieldName, fileName);
        String uploadId = blobResponse.getBlob().getUploadId();

        // open file to upload
        File file = new File(folderPath+File.separator+fileName);
        RandomAccessFile raf = new RandomAccessFile(file, "r");
        raf.seek(0);

        // upload chunks
        int bytesUploaded=0;
        while (bytesUploaded < raf.length()){
            // read chunk
            byte[] currentChunk;
            int bytesLeft = (int) (raf.length() - bytesUploaded);
            currentChunk = (bytesLeft > CHUNK_SIZE)
                    ? new byte[CHUNK_SIZE]
                    : new byte[bytesLeft];
            raf.read(currentChunk);

            uploadChunk(uploadId, currentChunk, bytesUploaded, currentChunk.length);

            // move marker in file
            bytesUploaded += currentChunk.length;
            raf.seek(bytesUploaded);
        }
        raf.close();

        return commitUpload(uploadId);
    }

    /**
     * Upload a local file to Chino.io as a BLOB. This method fully handles the upload of a BLOB.
     *
     * @param folderPath the path to the folder which contains the local file
     * @param document the Chino.io {@link Document} which contains this BLOB
     * @param fieldName the name of the field (of type "blob") that refers to this BLOB in the Document
     * @param fileName the name of the local file to upload
     *
     * @return A {@link CommitBlobUploadResponse} that wraps a {@link CommitBlobUploadResponseContent},
     * with information on the blob itself.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public CommitBlobUploadResponse uploadBlob(String folderPath, Document document, String fieldName, String fileName)
            throws IOException, ChinoApiException
    {
        return uploadBlob(folderPath, document.getDocumentId(), fieldName, fileName);
    }

    /**
     * Upload a local file to Chino.io as a BLOB, read from an {@link InputStream}. This method fully handles the
     * upload of a BLOB but does NOT close the stream. You should close it to prevent okhttp3 to warn about leaks.
     *
     * @param fileData an {@link InputStream} where the binary data to upload can be read from.
     *                 <b>WARNING:</b> the Stream must be {@link InputStream#close() closed} in order for the SDK to
     *                 complete the upload.
     * @param documentId the ID of the Chino.io {@link Document} which contains this BLOB
     * @param fieldName the name of the field (of type "blob") that refers to this BLOB in the Document
     * @param fileName the name of the file after the upload
     *
     * @return A {@link CommitBlobUploadResponse} that wraps a {@link CommitBlobUploadResponseContent},
     * with information on the blob itself.
     *
     * @throws IOException data processing error, e.g.: the {@link InputStream} was closed before the file was read
     * @throws ChinoApiException server error
     */
    public CommitBlobUploadResponse uploadBlob(InputStream fileData, String documentId, String fieldName, String fileName)
            throws IOException, ChinoApiException
    {
        checkNotNull(fileData, "file data stream");

        // get upload ID for uploading chunks
        CreateBlobUploadResponse blobResponse = initUpload(documentId, fieldName, fileName);
        String uploadId = blobResponse.getBlob().getUploadId();

        // upload chunks
        byte[] currentChunk = new byte[CHUNK_SIZE];
        int bytesUploaded=0;
        // read next chunk, max size: CHUNK_SIZE
        int bytesRead = fileData.read(currentChunk);
        while (bytesRead >= 0) {
            switch (bytesRead) {
                case CHUNK_SIZE:
                    // upload a full chunk
                    uploadChunk(uploadId, currentChunk, bytesUploaded, CHUNK_SIZE);
                    break;
                case 0: // no data in the current chunk, but the stream has not ended yet
                    break;
                default:
                    // upload a smaller chunk
                    byte[] smallChunk = new byte[bytesRead];
                    System.arraycopy(currentChunk, 0, smallChunk, 0, bytesRead);
                    uploadChunk(uploadId, smallChunk, bytesUploaded, bytesRead);
                    break;
            }
            // update offset0
            bytesUploaded += bytesRead;
            // read next chunk
            bytesRead = fileData.read(currentChunk);
        }

        return commitUpload(uploadId);
    }

    /**
     * Upload a local file to Chino.io as a BLOB, read from an {@link InputStream}. This method fully handles the
     * upload of a BLOB but does NOT close the stream. You should close it to prevent okhttp3 to warn about leaks.
     *
     * @param fileData an {@link InputStream} where the binary data to upload can be read from
     * @param document the Chino.io {@link Document} which contains this BLOB
     * @param fieldName the name of the field (of type "blob") that refers to this BLOB in the Document
     * @param fileName the name of the file after the upload
     *
     * @return A {@link CommitBlobUploadResponse} that wraps a {@link CommitBlobUploadResponseContent},
     * with information on the blob itself.
     *
     * @throws IOException data processing error, e.g.: the {@link InputStream} was closed before the file was read
     * @throws ChinoApiException server error
     */
    public CommitBlobUploadResponse uploadBlob(InputStream fileData, Document document, String fieldName, String fileName)
            throws IOException, ChinoApiException
    {
        return uploadBlob(fileData, document.getDocumentId(), fieldName, fileName);
    }

    /**
     * Get the BLOB with the specified {@code blobId} and store it on file system.
     *
     * @param blobId the id of the blob to retrieve
     * @param destination the path to a file where the BLOB will be saved
     *
     * @return {@link GetBlobResponse} which contains information about the downloaded BLOB
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     * @throws NoSuchAlgorithmException can't find MD5 / SHA algorithm (it's likely to be an issue with the
     *                                  dependencies or your Java installation)
     */
    public GetBlobResponse get(String blobId, String destination)
            throws IOException, ChinoApiException, NoSuchAlgorithmException
    {
        return this.get(blobId, destination, null);
    }

    /**
     * Get the BLOB with the specified {@code blobId} and store it on file system.
     *
     * @param blobId the id of the blob to retrieve
     * @param destination the path to a file where the BLOB will be saved
     * @param fileName name of the downloaded file. If {@code null} is provided, the file name will be inferred from
     *                 the response headers. If the headers contain no file name, a default will be used
     *                 ({@code blob-<blob-id>})
     *
     * @return {@link GetBlobResponse} which contains information about the downloaded BLOB
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     * @throws NoSuchAlgorithmException can't find MD5 / SHA algorithm (it's likely to be an issue with the
     *                                  dependencies or your Java installation)
     */
    public GetBlobResponse get(String blobId, String destination, String fileName)
            throws IOException, ChinoApiException, NoSuchAlgorithmException
    {
        checkNotNull(blobId, "blob_id");
        checkNotNull(destination, "destination");

        Request request = new Request.Builder().url(hostUrl + "/blobs/" + blobId).get().build();
        Response response = parent.getHttpClient().newCall(request).execute();

        if (response.code() != 200) {
            ErrorResponse error = new ObjectMapper().readValue(
                    response.body().byteStream(),
                    ErrorResponse.class);
            throw new ChinoApiException(error);
        }

        if (fileName == null) {
            // Read the name of the file from the "Content-Disposition" HTTP header
            // or - if not found - assign the default name "blob-<blobId>"
            fileName = getFileNameFromHeader(response, blobId);
        }

        return Blobs.writeToFile(response, destination, fileName);
    }

    /**
     * Retrieves a blob using a unique BLOB link.
     *
     * @param blobLink link to a BLOB, which can be obtanied by calling
     *                 {@link #generateLink(String, int, boolean) generateLink}
     * @param folderName the path to a folder where the BLOB will be saved
     * @param fileName the local name of the downloaded BLOB file. If {@code null}, a default name will be assigned
     *                 ({@code blob-<blob-id>})
     * @param mimeType the custom MIME Type of the retrieved object. If {@code null},
     *                 the MIME type will be inferred automatically.
     *
     * @return a {@link GetBlobResponse} if the retrieval is successful
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     * @throws NoSuchAlgorithmException can't find MD5 / SHA algorithm (it's likely to be an issue with the
     *                                  dependencies or your Java installation)
     *
     * @see #getFromLink(String, String, String) Automatically infer the MIME type
     * @see #generateLink(String, int, boolean)
     * @see #generateLink(String, int)
     * @see #generateLink(String)
     */
    public GetBlobResponse getFromLink(String blobLink, String folderName, String fileName, String mimeType)
            throws IOException, ChinoApiException, NoSuchAlgorithmException
    {
        checkNotNull(blobLink, "BLOB URL");
        checkNotNull(folderName, "destination");
        if (! blobLink.contains("token=")) {
            // just give a warning to inform of the error, but then go on anyways
            System.err.println("WARNING: blobs.getFromLink() received a BLOB URL without an access token." +
                    " BLOB URLs must contain a 'token' parameter.");
        }

        if (mimeType != null) {
            blobLink += "&mimetype=" + mimeType;
        }

        Request request = new Request.Builder().url(blobLink).get().build();
        Response response = parent.getHttpClient().newCall(request).execute();

        if (response.code() != 200) {
            ErrorResponse error = new ObjectMapper().readValue(
                    response.body().byteStream(),
                    ErrorResponse.class);
            throw new ChinoApiException(error);
        }

        if (fileName == null) {
            // compute default fileName (Content-Disposition headers are not available when using the link,
            // so the default name is always used here). First, we extract the <blobId> token from the URL:
            // https://api.test.chino.io/v1/blobs/url/<blobId>?token=<token>
            String blobId = blobLink.split("/blobs/url/")[1];
            blobId = blobId.split("\\?")[0];
            fileName = getFileNameFromHeader(response, blobId);
        }

        return Blobs.writeToFile(response, folderName, fileName);
    }

    /**
     * Retrieves a blob using a unique BLOB link.
     *
     * @param blobLink link to a BLOB, which can be obtanied by calling
     *                 {@link #generateLink(String, int, boolean) generateLink}
     * @param folderName the path to a file where the BLOB will be saved
     * @param fileName the local name of the downloaded BLOB file. If {@code null}, a default name will be assigned
     *                 ({@code blob-<blob-id>})
     *
     * @return a {@link GetBlobResponse} if the retrieval is successful
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     * @throws NoSuchAlgorithmException can't find MD5 / SHA algorithm (it's likely to be an issue with the
     *                                  dependencies or your Java installation)
     *
     * @see #generateLink(String, int, boolean)
     * @see #generateLink(String, int)
     * @see #generateLink(String)
     */
    public GetBlobResponse getFromLink(String blobLink, String folderName, String fileName)
            throws NoSuchAlgorithmException, ChinoApiException, IOException
    {
        return this.getFromLink(blobLink, folderName, fileName, null);
    }

    /**
     * Get an {@link InputStream} that can be used to read the BLOB with the specified {@code blobId} as a byte stream.
     * You should close it after you are done reading data to prevent okhttp3 to warn about leaks.
     *
     * @param blobId the id of the blob to retrieve
     *
     * @return an {@link InputStream}, as returned by {@link ResponseBody#byteStream()}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public InputStream getByteStream(String blobId) throws IOException, ChinoApiException {
        checkNotNull(blobId, "blob_id");

        // read blob as InputStream
        Request request = new Request.Builder().url(hostUrl + "/blobs/" + blobId).get().build();
        Response response = parent.getHttpClient().newCall(request).execute();
        InputStream responseStream = response.body().byteStream();

        // check response is successful
        if (response.code() != 200) {
            ErrorResponse error = new ObjectMapper().readValue(responseStream, ErrorResponse.class);
            throw new ChinoApiException(error);
        }
        return responseStream;
    }

    /**
     * Get an {@link InputStream} using a unique BLOB link.
     * The stream can be used to read the BLOB as a byte stream. You should close it after you are done reading data
     * to prevent okhttp3 to warn about leaks.
     *
     * @param blobLink link to a BLOB, which can be obtanied by calling
     *                 {@link #generateLink(String, int, boolean) generateLink}
     *
     * @return a {@link GetBlobResponse} if the retrieval is successful
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see #getByteStreamFromLink(String) Automatically infer the MIME type
     * @see #generateLink(String, int, boolean)
     * @see #generateLink(String, int)
     * @see #generateLink(String)
     */
    public InputStream getByteStreamFromLink(String blobLink, String mimeType) throws IOException, ChinoApiException {
        checkNotNull(blobLink, "BLOB_URL");
        if (! blobLink.contains("token=")) {
            // just give a warning to inform of the error, but then go on anyways
            System.err.println("WARNING: blobs.getFromLink() received a BLOB URL without an access token." +
                    " BLOB URLs must contain a 'token' parameter.");
        }

        if (mimeType != null) {
            blobLink += "&mimetype=" + mimeType;
        }

        // read blob as InputStream
        Request request = new Request.Builder().url(blobLink).get().build();
        Response response = parent.getHttpClient().newCall(request).execute();
        InputStream responseStream = response.body().byteStream();

        // check response is successful
        if (response.code() != 200) {
            ErrorResponse error = new ObjectMapper().readValue(responseStream, ErrorResponse.class);
            throw new ChinoApiException(error);
        }
        return responseStream;
    }

    /**
     * Get an {@link InputStream} using a unique BLOB link.
     * The stream can be used to read the BLOB as a byte stream. You should close it after you are done reading data
     * to prevent okhttp3 to warn about leaks.
     *
     * @param blobLink link to a BLOB, which can be obtanied by calling
     *                 {@link #generateLink(String, int, boolean) generateLink}
     *
     * @return a {@link GetBlobResponse} if the retrieval is successful
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see #generateLink(String, int, boolean)
     * @see #generateLink(String, int)
     * @see #generateLink(String)
     */
    public InputStream getByteStreamFromLink(String blobLink) throws IOException, ChinoApiException {
        return this.getByteStreamFromLink(blobLink, null);
    }

    /**
     * Create a BLOB object on Chino.io and initialize the metadata of the file that will be uploaded.
     * This method must always be invoked <b>before</b> {@link #uploadChunk(String, byte[], int, int)}.
     *
     * @see #uploadBlob(String, String, String, String)
     *
     * @param documentId the Chino.io ID of the {@link Document} the new BLOB belongs to. Must contain a field of type
     *                   "blob"
     * @param field the name of the "blob" field in the Document
     * @param fileName the name that will be given to the uploaded file
     *
     * @return a {@link CreateBlobUploadResponse}, which contains the upload_id that is needed for upload the file.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public CreateBlobUploadResponse initUpload(String documentId, String field, String fileName)
            throws IOException, ChinoApiException
    {
        CreateBlobUploadRequest createBlobUploadRequest=new CreateBlobUploadRequest(documentId, field, fileName);
        JsonNode data = postResource("/blobs", createBlobUploadRequest);
        if(data!=null)
            return mapper.convertValue(data, CreateBlobUploadResponse.class);
        return null;
    }

    /**
     * Upload a chunk of data to the server
     *
     * @see #uploadBlob(String, String, String, String)
     *
     * @param uploadId the upload ID returned by {@link #initUpload(String, String, String) initUpload()}
     * @param chunkData a chunk of data in the form of a byte array
     * @param offset the offset of the chunk relative to the beginning of the file
     * @param length the length of the chunk
     *
     * @return A {@link CreateBlobUploadResponse}, which contains the status of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public CreateBlobUploadResponse uploadChunk(String uploadId, byte[] chunkData, int offset, int length)
            throws IOException, ChinoApiException
    {
        JsonNode data = putResource("/blobs/"+uploadId, chunkData, offset, length);
        if(data!=null)
            return mapper.convertValue(data, CreateBlobUploadResponse.class);
        return null;
    }

    /**
     * Confirm and finalize the upload on Chino.io.
     * Make sure that <b>every chunk</b> has been successfully uploaded with
     * {@link #uploadChunk(String, byte[], int, int) uploadChunk()} before calling this method.
     *
     * @see #uploadBlob(String, String, String, String)
     *
     * @param uploadId the upload ID returned by {@link #initUpload(String, String, String) initUpload}
     *
     * @return A {@link CommitBlobUploadResponse}, which contains the status of the operation
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public CommitBlobUploadResponse commitUpload(String uploadId) throws IOException, ChinoApiException {
        CommitBlobUploadRequest commitBlobUploadRequest = new CommitBlobUploadRequest(uploadId);
        JsonNode data = postResource("/blobs/commit", commitBlobUploadRequest);
        if(data!=null)
            return mapper.convertValue(data, CommitBlobUploadResponse.class);
        return null;
    }

    /**
     * Generate a unique link that allows to access a BLOB without Chino.io credentials and returns the link metadata,
     * which include the security token and the expiration date. To get the BLOB link directly,
     * use {@link #generateLink(String, int, boolean)}
     *
     * @param blobId The ID of the BLOB to make accessible
     * @param duration The validity of the link (the value indicates <b>minutes</b>). Must be &gt;0.
     * @param oneTimeAccess If {@code true} is provided, the link will only allow one access, regardless of the
     *                      specified {@code duration}
     *
     * @return an instance of {@link BlobURLMetadata} with the security token, the expiration date and a flag to
     *         indicate whether or not the BLOB link can be accessed multiple times. Use method
     *         {@link BlobURLMetadata#getFullBlobLink(String, String)} to generate the direct link.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public BlobURLMetadata generateLinkMetadata(String blobId, int duration, boolean oneTimeAccess) throws IOException, ChinoApiException {
        // URL
        String path = String.format("/blobs/%s/generate", blobId);
        // body
        HashMap<String, Object> params = new HashMap<>();
        params.put("duration", duration);
        if (oneTimeAccess) {
            params.put("one_time", true);
        }
        // send API request (POST) and serialize response
        JsonNode data = postResource(path, params);
        if (data != null)
            return mapper.convertValue(data, BlobURLMetadata.class);

        return null;
    }

    /**
     * Generate a unique link that allows to access a BLOB without Chino.io credentials and returns the link metadata,
     * which include the security token and the expiration date. To get the BLOB link directly,
     * use {@link #generateLink(String, int, boolean)}
     *
     * @param blobId The ID of the BLOB to make accessible
     * @param duration The validity of the link (the value indicates <b>minutes</b>). Must be &gt;0.
     *
     * @return an instance of {@link BlobURLMetadata} with the security token, the expiration date and a flag to
     *         indicate whether or not the BLOB link can be accessed multiple times. Use method
     *         {@link BlobURLMetadata#getFullBlobLink(String, String)} to generate the direct link.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see #generateLinkMetadata(String, int, boolean) generateLinkMetadata(blobId, duration, oneTimeAccess)
     */
    public BlobURLMetadata generateLinkMetadata(String blobId, int duration) throws IOException, ChinoApiException {
        return this.generateLinkMetadata(blobId, duration, false);
    }

    /**
     * Generate a unique link that allows to access a BLOB without Chino.io credentials and returns the link metadata,
     * which include the security token and the expiration date. To get the BLOB link directly,
     * use {@link #generateLink(String, int, boolean)}
     *
     * To increase the validity period of the link, use {@link #generateLinkMetadata(String, int)} or
     * {@link #generateLinkMetadata(String, int, boolean)} instead.
     *
     * @return an instance of {@link BlobURLMetadata} with the security token, the expiration date and a flag to
     *         indicate whether or not the BLOB link can be accessed multiple times. Use method
     *         {@link BlobURLMetadata#getFullBlobLink(String, String)} to generate the direct link.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see #generateLinkMetadata(String, int, boolean) generateLinkMetadata(blobId, duration, oneTimeAccess)
     * @see #generateLinkMetadata(String, int) generateLinkMetadata(blobId, duration)
     */
    public BlobURLMetadata generateLinkMetadata(String blobId) throws IOException, ChinoApiException {
        return this.generateLinkMetadata(blobId, 60, false);
    }

    /**
     * Generate a unique link that allows to access a BLOB without Chino.io credentials
     *
     * @param blobId The ID of the BLOB to make accessible
     * @param duration The validity of the link (the value indicates <b>minutes</b>). Must be &gt;0.
     * @param oneTimeAccess If {@code true} is provided, the link will only allow one access, regardless of the
     *                      specified {@code duration}
     *
     * @return a String containing the unique link to retrieve the BLOB.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String generateLink(String blobId, int duration, boolean oneTimeAccess) throws IOException, ChinoApiException {
        BlobURLMetadata metadata = this.generateLinkMetadata(blobId, duration, oneTimeAccess);
        return metadata.getFullBlobLink(hostUrl, blobId);
    }

    /**
     * Generate a unique link that allows to access a BLOB without Chino.io credentials.
     *
     * @param blobId The ID of the BLOB to make accessible
     * @param duration The validity of the link (the value indicates <b>minutes</b>). Must be &gt;0.
     *
     * @return a String containing the unique link to retrieve the BLOB.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see #generateLink(String, int, boolean) generateLink(blobId, duration, oneTimeAccess)
     */
    public String generateLink(String blobId, int duration) throws IOException, ChinoApiException {
        return this.generateLink(blobId, duration, false);
    }

    /**
     * Generate a unique link that allows to access a BLOB without Chino.io credentials <b>for 60 minutes</b>.
     *
     * To increase the validity period of the link, use {@link #generateLink(String, int)} or
     * {@link #generateLink(String, int, boolean)} instead.
     *
     * @return a String containing the unique link to retrieve the BLOB.
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     *
     * @see #generateLink(String, int, boolean) generateLinkMetadata(blobId, duration, oneTimeAccess)
     * @see #generateLink(String, int) generateLinkMetadata(blobId, duration)
     */
    public String generateLink(String blobId) throws IOException, ChinoApiException {
        return this.generateLink(blobId, 60, false);
    }

    /**
     * Delete a BLOB from Chino.io. This operation can not be undone.
     *
     * @param blobId the id of the BLOB to delete
     *
     * @return a String with the result of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String delete(String blobId) throws IOException, ChinoApiException {
        checkNotNull(blobId, "blob_id");
        // 'force' parameter is ignored for BLOBs
        return deleteResource("/blobs/"+blobId, false);
    }

    /**
     * Accepts a byte stream from the server and writes the data to file, computing the MD5 and SHA1 checksums.
     *
     * @param apiResponse the {@link Response} containing the BLOB data
     * @param folderPath file system folder where the file must be saved
     * @param fileName name of the file to store.
     *
     * @return a {@link GetBlobResponse} or {@code null} if the API response was not
     *
     * @throws IOException data processing error
     * @throws NoSuchAlgorithmException can't find MD5 / SHA algorithm (it's likely to be an issue with the
     *                                  dependencies or your Java installation)
     */
    private static GetBlobResponse writeToFile(Response apiResponse, String folderPath, String fileName)
            throws IOException, NoSuchAlgorithmException
    {
        checkNotNull(apiResponse, "apiResponse");
        checkNotNull(folderPath, "folderPath");
        checkNotNull(fileName, "fileName");

        GetBlobResponse getBlobResponse = new GetBlobResponse();
        try {
            getBlobResponse.setFilename(fileName);
            getBlobResponse.setPath(folderPath + File.separator + getBlobResponse.getFilename());

            // read bytes from response and write them to file
            InputStream returnStream = apiResponse.body().byteStream();

            File file = new File(getBlobResponse.getPath());
            file.getParentFile().mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(file);

            // Compute MD5 and SHA1 hashes while writing file to disk
            int read;
            byte[] bytes = new byte[8 * 1024];

            MessageDigest MD5Digest = MessageDigest.getInstance("MD5");
            MessageDigest SHA1Digest = MessageDigest.getInstance("SHA1");

            while ((read = returnStream.read(bytes)) != -1) {
                fileOutputStream.write(bytes, 0, read);
                MD5Digest.update(bytes, 0, read);
                SHA1Digest.update(bytes, 0, read);
            }
            fileOutputStream.close();
            returnStream.close();

            String SHA1 = SHA1Calc.getSHA1Checksum(SHA1Digest.digest());
            String MD5 = MD5Calc.getMD5Checksum(MD5Digest.digest());

            getBlobResponse.setSize(file.length());
            getBlobResponse.setSha1(SHA1);
            getBlobResponse.setMd5(MD5);
        } finally { // close response body to prevent warnings from OkHttp3
            apiResponse.body().close();
        }

        return getBlobResponse;
    }

    /**
     * Read the file name from the "Content-Disposition" header of a BLOB API response.
     * If the header can't be found, or if it doesn't contain the file name, a default name is returned in the form of
     * {@code blob-{{ blob ID }} }
     *
     * @param response the API response
     * @param blobId the blob ID (used to generate the default name)
     *
     * @return a file name for the BLOB in the {@code response}
     */
    private String getFileNameFromHeader(Response response, String blobId) {
        /*
         * The "Content-Disposition" header has this structure:
         *
         *     Content-Disposition: attachment; filename=chino_logo.jpg
         *
         * The value can contain either 'filename*' or 'filename': the former has precedence if both are present,
         * so we try to read them in order. Learn more:
         * https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Disposition#Syntax
         */
        String fileName = null;
        String contentDisposition = response.header("Content-Disposition");
        if (contentDisposition != null) {
            fileName = contentDisposition.contains("filename*")
                    ? contentDisposition.split("filename\\*=")[1] // discard everything before "filename*="
                    : null;
            if (fileName == null) {
                fileName = contentDisposition.contains("filename")
                        ? contentDisposition.split("filename=")[1] // discard everything before "filename="
                        : null;
            }
        }
        if (fileName == null) {
            fileName = "blob-" + blobId;
        }

        return fileName;
    }

    /**
     * Get the size of the chunks used by the {@code uploadBlob} methods
     *
     * @see #uploadBlob(String, Document, String, String)
     * @see #uploadBlob(String, String, String, String)
     * @see #uploadBlob(InputStream, Document, String, String)
     * @see #uploadBlob(InputStream, String, String, String)
     *
     * @return the size of a chunk in bytes
     */
    public static int getDefaultChunkSize() {
        return CHUNK_SIZE;
    }
}
