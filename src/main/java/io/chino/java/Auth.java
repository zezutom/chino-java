package io.chino.java;

import com.fasterxml.jackson.databind.JsonNode;
import io.chino.api.application.Application;
import io.chino.api.application.ClientType;
import io.chino.api.auth.LoggedUser;
import io.chino.api.auth.TokenInfo;
import io.chino.api.common.ChinoApiException;
import io.chino.api.common.ErrorResponse;
import io.chino.api.common.LoggingInterceptor;
import io.chino.api.user.GetUserResponse;
import io.chino.api.user.User;
import okhttp3.*;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.chino.java.ChinoAPI.userAgent;

/**
 * Manage authentication of the Java clients with Chino.io API both as an admin or a OAuth2 {@link User}.
 */
public class Auth extends ChinoBaseAPI {

    private final ChinoAPI parent;
    private final String hostUrl;

    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl      the base URL of the Chino.io API. For testing, use:<br>
     *                        {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     */
    public Auth(String baseApiUrl, ChinoAPI parentApiClient) {
        super(baseApiUrl, parentApiClient);
        parent = parentApiClient;
        hostUrl = baseApiUrl;
    }

    /**
     * Login with username/password to an {@link Application}.
     *
     * @param username the "username" of the user
     * @param password the "password" of the user
     * @param applicationId the "application_id"
     * @param applicationSecret the application_secret - if authenticating from
     * a {@link ClientType#PUBLIC "public"} application, pass an empty String: {@code ""}
     *
     * @return a {@link LoggedUser} object
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public LoggedUser loginWithPassword(String username, String password, final String applicationId, final String applicationSecret) throws IOException, ChinoApiException {
        checkNotNull(username, "username");
        checkNotNull(password, "password");
        checkNotNull(applicationId, "application_id");
        checkNotNull(applicationSecret, "application_secret");
        RequestBody formBody;
        if (applicationSecret.equals("")) {
            formBody = new FormBody.Builder()
                    .add("grant_type", "password")
                    .add("username", username)
                    .add("password", password)
                    .add("client_id", applicationId)
                    .build();
        } else {
            formBody = new FormBody.Builder()
                    .add("grant_type", "password")
                    .add("username", username)
                    .add("password", password)
                    .add("client_id", applicationId)
                    .add("client_secret", applicationSecret)
                    .build();
        }
        Request request = new Request.Builder()
                .url(hostUrl+"/auth/token/")
                .post(formBody)
                .build();
        parent.updateHttpAuth(null);
        Response response = parent.getHttpClient().newCall(request).execute();

        return parseTokensAndUpdateAuth(response);
    }
    
    /**
     * Login with username/password to a {@link ClientType#PUBLIC "public"}
     * Application. No need to know the {@link Application}'s "application_secret".
     *
     * @param username the "username" of the user
     * @param password the "password" of the user
     * @param applicationId the "application_id"
     *
     * @return a {@link LoggedUser} object
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error 
     */
    public LoggedUser loginWithPassword (String username, String password, final String applicationId) throws IOException, ChinoApiException {
        return loginWithPassword(username, password, applicationId, "");
    }

    /**
     * Save the {@code token} in the parent's {@link OkHttpClient} to be used in future calls.
     *
     * @param token the new token to be used in the API calls
     *
     * @return information about the current {@link User}'s status (see also {@link #checkUserStatus() checkUserStatus()})
     *
     * @throws IOException the User can not be found on server. Thrown by {@link Call#execute() okhttp3.Call}
     * @throws ChinoApiException server responded with error
     */
    public User loginWithBearerToken(String token) throws IOException, ChinoApiException {
        checkNotNull(token, "token");
        parent.updateHttpAuth(new LoggingInterceptor(token));
        return checkUserStatus();
    }

    /**
     * Login with authentication code for users
     *
     * @param code the code retrieved from the app server
     * @param redirectUrl the redirect_url of the app server
     * @param applicationId the id of the Application
     * @param applicationSecret the Application secret (pass an empty string if you login from a "public application")
     *
     * @return LoggedUser Object
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public LoggedUser loginWithAuthenticationCode(String code, String redirectUrl, final String applicationId, final String applicationSecret) throws IOException, ChinoApiException {
        checkNotNull(code, "code");
        checkNotNull(redirectUrl, "redirect_url");
        checkNotNull(applicationId, "application_id");
        checkNotNull(applicationSecret, "application_secret");
        RequestBody formBody;
        if (applicationSecret.equals("")) {
            formBody = new FormBody.Builder()
                    .add("grant_type", "authorization_code")
                    .add("code", code)
                    .add("redirect_uri", redirectUrl)
                    .add("client_id", applicationId)
                    .add("scope", "read write")
                    .build();
        } else {
            formBody = new FormBody.Builder()
                    .add("grant_type", "authorization_code")
                    .add("code", code)
                    .add("redirect_uri", redirectUrl)
                    .add("client_id", applicationId)
                    .add("client_secret", applicationSecret)
                    .add("scope", "read write")
                    .build();
        }
        Request request = new Request.Builder()
                .url(hostUrl+"/auth/token/")
                .post(formBody)
                .build();
        Response response = parent.getHttpClient().newCall(request).execute();
        return parseTokensAndUpdateAuth(response);
    }

    /**
     * Parses Chino.io server {@link Response response} and updates the parent's {@link OkHttpClient}
     * authentication method
     *
     * @param response the server's {@link Response}
     *
     * @return a {@link LoggedUser} contining the User's access tokens
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    private LoggedUser parseTokensAndUpdateAuth(Response response) throws IOException, ChinoApiException {
        LoggedUser loggedUser = parseResponseBody(response, LoggedUser.class);

        if (loggedUser == null) return null;

        // Update the HTTP authentication header for this client
        parent.updateHttpAuth(new LoggingInterceptor(loggedUser.getAccessToken()));
        return loggedUser;
    }

    /**
     * Converts the content of a {@link Response} to the desired data type
     *
     * @param response the okhttp3 {@link Response} object
     * @param targetClass the datatype to convert the data to
     *
     * @return a new instance of the target Class
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    private <T> T parseResponseBody(Response response, Class<T> targetClass) throws IOException, ChinoApiException {
        checkNotNull(response, "response");
        checkNotNull(response.body(), "response body");

        // convert response to String, then parse it and return an instance of the target Class
        String bodyString = response.body().string();
        checkNotNull(bodyString, "response body string");

        if (response.code() == 200) {
            // The actual response data are enclosed in a wrapper inside the 'data' field
            JsonNode data = mapper.readTree(bodyString).get("data");
            if (data != null) {
                // Convert to the target Class
                // noinspection UnnecessaryLocalVariable - makes code readable
                T obj = mapper.convertValue(data, targetClass);
                return obj;
            }
            // no data
            return null;
        } else {
            // body contains info about the error
            throw new ChinoApiException(mapper.readValue(bodyString, ErrorResponse.class));
        }
    }

    /**
     * It refreshes the token for the logged user
     *
     * @param refreshToken the refresh_token in the attributes of the logged user
     * @param applicationId the id of the Application
     * @param applicationSecret the Application secret
     *
     * @return LoggedUser Object updated
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public LoggedUser refreshToken(String refreshToken, final String applicationId, final String applicationSecret) throws IOException, ChinoApiException {
        checkNotNull(refreshToken, "refresh_token");
        checkNotNull(applicationId, "application_id");
        checkNotNull(applicationSecret, "application_secret");
        RequestBody formBody = new FormBody.Builder()
                .add("grant_type", "refresh_token")
                .add("refresh_token", refreshToken)
                .add("client_id", applicationId)
                .add("client_secret", applicationSecret)
                .build();
        Request request = new Request.Builder()
                .url(hostUrl + "/auth/token/")
                .post(formBody)
                .build();
        Response response = parent.getHttpClient().newCall(request).execute();
        return parseTokensAndUpdateAuth(response);
    }

    /**
     * It checks the logged user status
     *
     * @return User Object that contains the status of the logged user
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public User checkUserStatus() throws IOException, ChinoApiException {
        // Keep without trailing '/' or it won't work
        JsonNode data = getResource("/users/me");
        if(data!=null){
            return mapper.convertValue(data, GetUserResponse.class).getUser();
        }
        return null;
    }

    /**
     * Get information about an OAuth access_token
     *
     * @param accessToken the access token (a.k.a. bearer token) returned by Chino.io auth endpoints
     * @param applicationId app ID of a Chino.io {@link Application}
     * @param applicationSecret app secret of a Chino.io {@link Application}
     * @param getUuidAsUsername if {@code true}, the method {@link TokenInfo#getUsername()} will return the unique
     *                          <i>user_id</i> of the {@link User} instead of the username.
     *
     * @return an instance of {@link TokenInfo} containing details about the token and the related {@link User}
     *
     * @see #loginWithPassword(String, String, String)
     * @see #loginWithPassword(String, String, String, String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public TokenInfo introspectToken(String accessToken, String applicationId, String applicationSecret, boolean getUuidAsUsername) throws IOException, ChinoApiException {
        checkNotNull(accessToken, "bearer_token");

        String basicAuth = String.format("%s:%s", applicationId, applicationSecret);
        basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());

        // This flag asks the system to return the UUID of the user instead of the username in the "username" field
        // of the Token Introspect endpoint
        String params;
        if (getUuidAsUsername) {
            params = "?uid_type=uuid";
        } else {
            params = "";
        }

        RequestBody formBody = new FormBody.Builder()
                .add("token", accessToken)
                .build();
        Request request = new Request.Builder()
                .url(hostUrl + "/auth/introspect/" + params)
                .post(formBody)
                .addHeader("Authorization", String.format("Basic %s", basicAuth))
                .build();

        // We need to create another HTTP client to prevent the LoggingInterceptor from overwriting the auth headers
        OkHttpClient client = parent.getDefaultHttpClient().build();
        Response response = client.newCall(request).execute();
        return parseResponseBody(response, TokenInfo.class);
    }

    /**
     * Get information about an OAuth access_token<br>
     * It has the same effect as calling:
     * <pre>
     * <code>{@link #introspectToken(String, String, String, boolean) introspectToken}(accessToken, applicationId, applicationSecret, false)</code>.
     * </pre>
     *
     * @param accessToken the access token (a.k.a. bearer token) returned by Chino.io auth endpoints
     * @param applicationId app ID of a Chino.io {@link Application}
     * @param applicationSecret app secret of a Chino.io {@link Application}
     *
     * @return an instance of {@link TokenInfo} containing details about the token and the related {@link User}
     *
     * @see #loginWithPassword(String, String, String)
     * @see #loginWithPassword(String, String, String, String)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public TokenInfo introspectToken(String accessToken, String applicationId, String applicationSecret) throws IOException, ChinoApiException {
        return introspectToken(accessToken, applicationId, applicationSecret, false);
    }

        /**
         * Log out from a  {@link Application} using the user's access token, which will become invalid after the operation.
         * Use this method for {@link ClientType#CONFIDENTIAL CONFIDENTIAL} clients
         * or when you are unsure about the client type.
         *
         * @param token the access token of the logged user
         *
         * @return a String with the result of the operation
         *
         * @throws IOException data processing error
         * @throws ChinoApiException server error
         */
    public String logout(String token, final String applicationId, final String applicationSecret) throws IOException, ChinoApiException {
        checkNotNull(token, "token");
        checkNotNull(applicationId, "application_id");
        checkNotNull(applicationSecret, "application_secret");
        RequestBody formBody = new FormBody.Builder()
                .add("token", token)
                .add("client_id", applicationId)
                .add("client_secret", applicationSecret)
                .build();
        Request request = new Request.Builder()
                .url(hostUrl+"/auth/revoke_token/")
                .post(formBody)
                .build();
        // execute call with a new HTTP client. This operation is made in order to avoid
        // sending 2 different tokens, since the parent's client could be using another token
        Response response = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(userAgent)
                .build()
                .newCall(request).execute();

        // check if the revoked token is currently being used in the parent's HTTP client and, if so, remove it:
        List<Interceptor> interceptors = parent.getHttpClient().networkInterceptors();
        boolean resetAuth = false;
        for (Interceptor i:interceptors) {
            if (i instanceof LoggingInterceptor) {
                String interceptorToken = ((LoggingInterceptor) i).getAuthorization().replace("Bearer ", "");
                if (interceptorToken.equals(token))
                    resetAuth = true;
                break;
            }
        }
        if (resetAuth)
            parent.updateHttpAuth(null);

        String body = response.body().string();
        if (response.code() == 200) {
            return SUCCESS_MSG;
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * Log out from a {@link Application} using the user's access token, which will become invalid after the operation.
     * This method works only for {@link ClientType#PUBLIC PUBLIC} clients.
     * If you are not sure, use {@link #logout(String, String, String)}.
     *
     * @param token the access token of the logged user
     *
     * @return a String with the result of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String logout(String token, final String applicationId) throws IOException, ChinoApiException {
        return logout(token, applicationId, "");
    }
}


