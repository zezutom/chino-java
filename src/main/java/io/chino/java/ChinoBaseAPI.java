package io.chino.java;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.common.*;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

/**
 * Provides utility methods to communicate with Chino API, such as HTTP GET and POST calls.
 */
public class ChinoBaseAPI {

    public final static String SUCCESS_MSG = "success";

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    public static final MediaType OCTET_STREAM = MediaType.parse("application/octet-stream");
    private final ChinoAPI parent;
    private final String hostUrl;
    static ObjectMapper mapper;


    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl the base URL of the Chino.io API. For testing, use:<br>
     *     {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     */
    public ChinoBaseAPI(String baseApiUrl, ChinoAPI parentApiClient) {
        this.hostUrl = baseApiUrl;
        parent = parentApiClient;
        mapper = getMapper();
    }

    /**
     * The default function to make a POST call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode postResource(String path, Object resource) throws IOException, ChinoApiException {
        Response response = postResourceRaw(path, resource);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a POST call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response postResourceRaw(String path, Object resource) throws IOException {
        String json = mapper.writeValueAsString(resource);
        RequestBody requestBody = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(hostUrl + path)
                .post(requestBody)
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * It makes a POST call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @param offset the offset value in the request
     * @param limit the limit value in the request
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode postResource(String path, Object resource, int offset, int limit) throws IOException, ChinoApiException {
        Response response = postResourceRaw(path, resource, offset, limit);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * It makes a POST call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @param offset the offset value in the request
     * @param limit the limit value in the request
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response postResourceRaw(String path, Object resource, int offset, int limit) throws IOException {
        String json = mapper.writeValueAsString(resource);
        RequestBody requestBody = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(hostUrl+path+"?offset="+offset+"&limit="+limit)
                .post(requestBody)
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * The default function to make a GET call to the server saved in hostUrl.
     * This is the default method to get paginated results.
     * @param path the path of the URL
     * @param offset the offset value in the request
     * @param limit the limit value in the request
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode getResource(String path, int offset, int limit) throws IOException, ChinoApiException {
        Response response = getResourceRaw(path, offset, limit);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a GET call to the server saved in hostUrl.
     * This is the default method to get paginated results.
     * @param path the path of the URL
     * @param offset the offset value in the request
     * @param limit the limit value in the request
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response getResourceRaw(String path, int offset, int limit) throws IOException {
        Request request = new Request.Builder()
                .url(hostUrl + path+"?offset="+offset+"&limit="+limit)
                .get()
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }
    
    /**
     * The default function to make a GET call to the server saved in hostUrl.
     * This call allows to pass any value as a URL parameter with the call.
     * @param path the path of the URL
     * @param urlParameters a <code>&lt;</code><code>key, value</code><code>&gt;</code>
     * {@link HashMap} containing the URL parameters.
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode getResource(String path, HashMap<String, String> urlParameters) throws IOException, ChinoApiException {
        Response response = getResourceRaw(path, urlParameters);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a GET call to the server saved in hostUrl.
     * This call allows to pass any value as a URL parameter with the call.
     * @param path the path of the URL
     * @param urlParameters a <code>&lt;</code><code>key, value</code><code>&gt;</code>
     * {@link HashMap} containing the URL parameters.
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response getResourceRaw(String path, HashMap<String, String> urlParameters) throws IOException {
        // parse parameters
        StringBuilder paramString = new StringBuilder();
        if (!urlParameters.isEmpty()) {
            paramString = new StringBuilder("?");
            // Add all parameters after '?'
            int parametersLeftToAdd = urlParameters.keySet().size() - 1;
            for (String name : urlParameters.keySet()) {
                String value = urlParameters.get(name);
                paramString.append(name).append("=").append(value);
                // Append '&' unless this is the last parameter
                paramString.append((parametersLeftToAdd > 0) ? "&" : "");
                parametersLeftToAdd --;
            }
        }
        // send GET request
        Request request = new Request.Builder()
                .url(hostUrl + path + paramString)
                .get()
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * It makes a GET call to the server saved in hostUrl
     * @param path the path of the URL
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode getResource(String path) throws IOException, ChinoApiException {
        Response response = getResourceRaw(path);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * It makes a GET call to the server saved in hostUrl
     * @param path the path of the URL
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response getResourceRaw(String path) throws IOException {
        Request request = new Request.Builder()
                .url(hostUrl + path)
                .get()
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * The default function to make a PUT call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode putResource(String path, Object resource) throws IOException, ChinoApiException {
        Response response = putResourceRaw(path, resource);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a PUT call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response putResourceRaw(String path, Object resource) throws IOException {
        String json = mapper.writeValueAsString(resource);
        RequestBody requestBody = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(hostUrl+path)
                .put(requestBody)
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * The default function to make a PATCH call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @return JsonNode Object with the response of the server if there are no errors
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public JsonNode patchResource(String path, Object resource) throws IOException, ChinoApiException {
        Response response = patchResourceRaw(path, resource);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a PATCH call to the server saved in hostUrl
     * @param path the path of the URL
     * @param resource the Object that would be mapped in a JSON format for the request
     * @return the raw {@link Response} returned by the server
     * @throws IOException data processing error
     */
    public Response patchResourceRaw(String path, Object resource) throws IOException {
        String json = mapper.writeValueAsString(resource);
        RequestBody requestBody = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
                .url(hostUrl+path)
                .patch(requestBody)
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * The function used by Blob class to make a PUT call to upload the chunks
     *
     * @param path the path of the URL
     * @param resource the byte array of the chunk which needs to be uploaded
     * @param offset the offset value in the request
     * @param length the length value in the request
     *
     * @return JsonNode Object with the response of the server (if there are no errors)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    // TODO add all updatePartial methods to classes in io.chino.java
    public JsonNode putResource(String path, byte[] resource, int offset, int length) throws IOException, ChinoApiException {
        Response response = putResourceRaw(path, resource, offset, length);
        String body = response.body().string();
        if (response.code() == 200) {
            return mapper.readTree(body).get("data");
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The function used by Blob class to make a PUT call to upload the chunks
     *
     * @param path the path of the URL
     * @param resource the byte array of the chunk which needs to be uploaded
     * @param offset the offset value in the request
     * @param length the length value in the request
     *
     * @return the raw {@link Response} returned by the server
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Response putResourceRaw(String path, byte[] resource, int offset, int length) throws IOException, ChinoApiException {
        RequestBody requestBody = RequestBody.create(OCTET_STREAM, resource);

        Request request = new Request.Builder()
                .url(hostUrl+path)
                .header("offset", String.valueOf(offset))
                .header("length", String.valueOf(length))
                .put(requestBody)
                .build();

        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * The default function to make a DELETE call to the server saved in hostUrl
     * @param path the path of the URL
     * @param force the force parameter in the request
     * @return String with the result of the operation
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String deleteResource(String path, boolean force) throws IOException, ChinoApiException {
        Response response = deleteResourceRaw(path, force);
        String body = response.body().string();
        if (response.code() == 200) {
            return SUCCESS_MSG;
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a DELETE call to the server saved in hostUrl
     * @param path the path of the URL
     * @param params additional URL parameters
     * @return String with the result of the operation
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String deleteResource(String path, HashMap<String, String> params) throws IOException, ChinoApiException {
        Response response = deleteResourceRaw(path, params);
        String body = response.body().string();
        if (response.code() == 200) {
            return SUCCESS_MSG;
        } else {
            throw new ChinoApiException(mapper.readValue(body, ErrorResponse.class));
        }
    }

    /**
     * The default function to make a DELETE call to the server saved in hostUrl
     * 
     * @param path the URL to call
     * @param force the 'force' parameter in the delete request
     * 
     * @return the raw {@link Response} returned by the server
     * 
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Response deleteResourceRaw(String path, boolean force) throws IOException, ChinoApiException {
        Request request;
        if (force) {
            request = new Request.Builder()
                    .url(hostUrl + path+"?force=true")
                    .delete()
                    .build();
        } else {
            request = new Request.Builder()
                    .url(hostUrl + path)
                    .delete()
                    .build();
        }
        return parent.getHttpClient().newCall(request).execute();
    }

    /**
     * The default function to make a DELETE call to the server saved in hostUrl
     * 
     * @param path the URL to call
     * @param urlParameters the parameters to be sent in the request URL
     * 
     * @return the raw {@link Response} returned by the server
     * 
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public Response deleteResourceRaw(String path, HashMap<String, String> urlParameters) throws IOException, ChinoApiException {
        // parse parameters
        StringBuilder paramString = new StringBuilder();
        if (!urlParameters.isEmpty()) {
            paramString = new StringBuilder("?");
            // Add all parameters after '?'
            int parametersLeftToAdd = urlParameters.keySet().size() - 1;
            for (String name : urlParameters.keySet()) {
                String value = urlParameters.get(name);
                paramString.append(name).append("=").append(value);
                // Append '&' unless this is the last parameter
                paramString.append((parametersLeftToAdd > 0) ? "&" : "");
                parametersLeftToAdd --;
            }
        }
        // send DELETE request
        Request request = new Request.Builder()
                .url(hostUrl + path + paramString)
                .delete()
                .build();
        return parent.getHttpClient().newCall(request).execute();
    }

    // This function is static because there are some classes in io.chino.api that need an ObjectMapper
    // without instantiating a new ChinoBaseAPI()
    public static ObjectMapper getMapper() {
        if(mapper==null) {
            mapper = new ObjectMapper();
        }

        return mapper;
    }

    /*
     *  This is a function used in both Schema and UserSchema. It is used when a custom class is passed in the
     *  constructor to create or update a new Schema or UserSchema, and all the Fields in such class are checked.
     *  This function returns a String of the type of the Field passed.
     *
     *  Arrays are allowed, but just for 'string', 'integer' and 'float'.
     */

    /**
     * Converts Java native
     *
     * @param type a Java {@link Class} or native type.
     *
     * @return a {@link String} with the related {@link Field} type
     *
     * @throws ChinoApiException invalid type
     */
    protected String getChinoType(Class type) throws ChinoApiException {
        String typeStr;

        if (type == null) {
            throw new ChinoApiException("error, unsupported type: null");
        }

        if (type == String.class || type.getComponentType() == String.class) {
            typeStr = "string";
        } else if (type == int.class || type == Integer.class || type.getComponentType() == int.class || type.getComponentType() == Integer.class) {
            typeStr = "integer";
        } else if (type == float.class || type == Float.class || type.getComponentType() == float.class || type.getComponentType() == Float.class) {
            typeStr = "float";
        } else if (type == boolean.class || type == Boolean.class) {
            typeStr = "boolean";
        } else if (type == Date.class || (type == java.sql.Date.class) ) {
            typeStr = "date";
        } else if (type == Time.class) {
            typeStr = "time";
        } else if (type == Timestamp.class || type == java.security.Timestamp.class) {
            typeStr = "datetime";
        } else if (type == File.class) {
            typeStr = "blob";
        } else if (type == Base64.class) {
            typeStr = "base64";
        } else if (type == HashMap.class || type == LinkedHashMap.class || type == JsonNode.class) {
            typeStr = "json";
        } else {
            throw new ChinoApiException("error, unsupported type for conversion: " + type.getName() + ".");
        }

        String returnStr = (type.getComponentType() != null) ? "array[%s]" : "%s";
        return String.format(returnStr, typeStr);
    }

    /**
     * Used for the Reflection operation when a custom class is passed as argument to create or update a Schema or a UserSchema to retrieve the fields in the class
     *
     */
    protected List<Field> returnFields(Class myClass) throws ChinoApiException {
        checkNotNull(myClass, "my_class");
        java.lang.reflect.Field[] fields = myClass.getDeclaredFields();
        List<Field> fieldsList= new ArrayList<>();
        for(java.lang.reflect.Field field : fields) {
            try {
                // ignore transient fields, i.e. fields used for test coverage with Jacoco
                if (! Modifier.isTransient(field.getModifiers())) {
                    String chinoType = getChinoType(field.getType());
                    if (chinoType != null) {
                        if (field.getAnnotation(indexed.class) != null) {
                            fieldsList.add(new Field(field.getName(), chinoType, true));
                        } else {
                            fieldsList.add(new Field(field.getName(), chinoType));
                        }
                    }
                }
            } catch (ChinoApiException e) {
                String msg = String.format("Failed to convert field '%s' of class '%s'", field.getName(), myClass.getCanonicalName());
                ChinoApiException wrapper = new ChinoApiException(msg);
                wrapper.initCause(e);
                throw wrapper;
            }
        }
        return fieldsList;
    }

    protected HashMap<String, Object> fromStringToHashMap(String value) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        //convert JSON string to Map
        return mapper.readValue(
                value,
                new TypeReference<HashMap<String, Object>>() {}
        );
    }

    /**
     * Verify that the {@link Object} is not a {@code null} reference; otherwise
     * interrupts execution with an exception, specifying the name of the {@code null} Object.
     *
     * @param object an {@link Object}
     * @param name a name that identifies the Object for the dev
     *
     * @throws NullPointerException the object (whose name is reported in the Exception) is {@code null}.
     */
    protected static void checkNotNull(Object object, String name) {
        if (object == null) {
            throw new NullPointerException(name);
        }
    }

    /**
     * Verify that every {@link Pair} in the varargs contains a non-{@code null} {@link Object} reference; otherwise
     * interrupts execution with an exception, specifying the name of the {@code null} Object.
     *
     * @param elements a list of {@link Pair} where the first element is an {@link Object}
     *                 and the second is a {@link String}.
     *
     * @see #checkNotNull(Object, String)
     *
     * @throws NullPointerException one of the objects (whose name is reported in the Exception message) is {@code null}
     */
    @SafeVarargs
    protected static void checkNotNull(Pair<Object, String>... elements) {
        for (Pair<Object, String> element : elements) {
            checkNotNull(element.getKey(), element.getValue());
        }
    }
}
