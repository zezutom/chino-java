package io.chino.java;

import com.fasterxml.jackson.databind.JsonNode;
import io.chino.api.common.ChinoApiConstants;
import io.chino.api.common.ChinoApiException;
import io.chino.api.document.Document;
import io.chino.api.group.Group;
import io.chino.api.permission.*;
import io.chino.api.user.User;

import java.io.IOException;

/**
 * Manage access Permissions over Chino.io resources.
 * For instructions about how to use the Permissions system, visit the
 * <a href="https://docs.test.chino.io/custodia/docs/v1#permissions">Permissions</a> page on
 * the Chino.io API docs.
 *
 * @see PermissionsRequest
 * @see PermissionsRequestBuilder
 * @see PermissionSetter
 */
public class Permissions extends ChinoBaseAPI {

    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl      the base URL of the Chino.io API. For testing, use:<br>
     *                        {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     */
    public Permissions(String baseApiUrl, ChinoAPI parentApiClient) {
        super(baseApiUrl, parentApiClient);
    }


    /* NEW PERMISSIONS INTERFACE */

    /**
     * Create a new {@link PermissionsRequest} that will grant new Permissions over resources in Chino.io.
     *
     * @return a {@link PermissionsRequestBuilder} that can be used to specify the subject, the target resource(s)
     * and the new Permissions that will be granted.
     */
    public PermissionsRequestBuilder grant() {
        return new PermissionsRequestBuilder(Action.GRANT, this);
    }

    /**
     * Create a new {@link PermissionsRequest} that will revoke existing Permissions over resources in Chino.io.
     *
     * @return a {@link PermissionsRequestBuilder} that can be used to specify the subject, the target resource(s)
     * and the new Permissions that will be revoked.
     */
    public PermissionsRequestBuilder revoke() {
        return new PermissionsRequestBuilder(Action.REVOKE, this);
    }

    /**
     * Execute a {@link PermissionsRequest}
     *
     * @param request a {@link PermissionsRequest}
     *
     * @return a String with the result of the operation
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public String executeRequest(PermissionsRequest request) throws IOException, ChinoApiException {
        postResource(request.getUrlPath(), request.getBody());
        return SUCCESS_MSG;
    }

    /**
     * Read all the Permissions set on every resource belonging to a logged {@link User User}.
     * Doesn't work if invoked by a {@link ChinoAPI#ChinoAPI(String, String, String) customer client}
     * (i.e. authenticated with customerId and customerKey)
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException either a server error or this method was invoked by a customer client
     */
    public GetPermissionsResponse readPermissions() throws IOException, ChinoApiException {
        JsonNode data = getResource("/perms", 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return mapper.convertValue(data, GetPermissionsResponse.class);

        return null;
    }

    /**
     * Reads all Permissions of a given {@link User}. In the response are omitted all
     * the Permissions on Documents, because they would generate too many results.<br>
     * <br>
     * Customers can always perform this call, while {@link User Users} can only
     * see their own Permissions.
     *
     * @param user a {@link User}
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error, or the User is unauthorized to see these permissions.
     */
    public GetPermissionsResponse readPermissions(User user) throws IOException, ChinoApiException {
        return readPermissionsOfaUser(user.getUserId());
    }

    /**
     * Reads all Permissions of a given {@link Group}. In the response are omitted all
     * the Permissions on Documents, because they would generate too many results.<br>
     * <br>
     * Customers can always perform this call, while {@link User Users} can only
     * specify the ID of a {@link Group} they belong to.
     *
     * @param group a Chino.io {@link Group}
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error, or the User doesn't belong to the specified Group.
     */
    public GetPermissionsResponse readPermissions(Group group) throws IOException, ChinoApiException {
        return readPermissionsOfaGroup(group.getGroupId());
    }

    /**
     * Reads Permissions that are set on a single {@link io.chino.api.document.Document Document}.
     * <br>
     * <br>
     * Customers can always perform this call, while {@link User Users} must have
     * {@code R} Permission on the document.
     *
     * @param document a Chino.io {@link io.chino.api.document.Document Document}
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetPermissionsResponse readPermissionsOn(Document document) throws IOException, ChinoApiException {
        return readPermissionsOnaDocument(document.getDocumentId());
    }

    /**
     * Reads Permissions that are set on a single {@link io.chino.api.document.Document Document}.<br>
     * <br>
     * Customers can always perform this call, while {@link User Users} must have
     * {@code R} Permission on the document.
     *
     * @param documentId the id of a {@link io.chino.api.document.Document Document} on Chino.io
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @see #readPermissionsOn(Document)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error
     */
    public GetPermissionsResponse readPermissionsOnaDocument(String documentId) throws IOException, ChinoApiException {
        checkNotNull(documentId, "document_id");
        JsonNode data = getResource("/perms/documents/"+documentId, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null) {
            return mapper.convertValue(data, GetPermissionsResponse.class);
        }
        return null;
    }


    /**
     * Reads all Permissions of a given {@link User User}. In the response are omitted all
     * the Permissions on Documents, because they would generate too many results.<br>
     * <br>
     * Customers can always perform this call, while {@link User Users} can only
     * see their own Permissions.
     *
     * @param userId the id of a {@link User User} on Chino.io
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @see #readPermissions(User)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error, or the User is unauthorized to see these permissions.
     */
    public GetPermissionsResponse readPermissionsOfaUser(String userId) throws IOException, ChinoApiException{
        checkNotNull(userId, "user_id");
        JsonNode data = getResource("/perms/users/"+userId, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return mapper.convertValue(data, GetPermissionsResponse.class);

        return null;
    }


    /**
     * Reads all Permissions of a given {@link Group Group}. In the response are omitted all
     * the Permissions on Documents, because they would generate too many results.<br>
     * <br>
     * Customers can always perform this call, while {@link User Users} can only
     * specify the ID of a {@link Group Group} they belong to.
     *
     * @param groupId the ID of a {@link Group Group} on Chino.io
     *
     * @return A {@link GetPermissionsResponse} that wraps a list of {@link io.chino.api.permission.Permission Permissions}
     *
     * @see #readPermissions(Group)
     *
     * @throws IOException data processing error
     * @throws ChinoApiException server error, or the User doesn't belong to the specified Group.
     */
    public GetPermissionsResponse readPermissionsOfaGroup(String groupId) throws IOException, ChinoApiException{
        checkNotNull(groupId, "group_id");
        JsonNode data = getResource("/perms/groups/"+groupId, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT);
        if(data!=null)
            return mapper.convertValue(data, GetPermissionsResponse.class);

        return null;
    }

    /* MACROS */

    /**
     *  The types of Permission that can be granted/revoked on a resource.
     */
    public enum Type {
        /** Create resources - only applies to resource types (e.g. all Documents in a Schema) */
        CREATE,

        /** Read resources - applies to a specific resource or to a type (e.g. all Documents in a Schema) */
        READ,

        /** Update resources - applies to a specific resource or to a type (e.g. all Documents in a Schema) */
        UPDATE,

        /** Delete resources - applies to a specific resource or to a type (e.g. all Documents in a Schema) */
        DELETE,

        /** List resources - only applies to resource types (e.g. all Documents in a Schema) */
        LIST,

        /**
         * Administer permissions - can only be set in the "authorize" Permissions list of a user.
         * Enables the user to add/remove permissions he can "manage" from/to other users' "authorize" list.
         */
        ADMIN,

        /**
         * Search content - only applies to {@link io.chino.api.schema.Schema Schemas} and
         * {@link io.chino.api.userschema.UserSchema UserSchemas}, with the following rules: (click on the constant to see full Javadoc)<br>
         * <ul>
         *     <li>
         *         On a Schema, allows to search <b>all the {@link io.chino.api.document.Document Documents}
         *         he has {@link #READ} permission over</b>.
         *     </li>
         *     <li>
         *         On a UserSchema, allows to search <b>every user</b> in the schema.
         *     </li>
         * </ul>
         */
        SEARCH;

        @Override
        public String toString() {
            return name().substring(0, 1).toUpperCase();
        }

        public static Type fromString(String type) {
            for (Type value : values()) {
                if (value.toString().equals(type)) {
                    return value;
                }
            }
            throw new IllegalArgumentException(type + " is not a valid permission type string.");
        }
    }

    /**
     *  Actions that modify Permissions of a {@link Subject} over one or multiple resources.
     */
    public enum Action {
        /** {@code action}: grant permission */
        GRANT,
        /** {@code action}: revoke permission */
        REVOKE;

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }
    }

    /**
     * The resources of Chino.io. Access to resources is regulated with Permissions.
     */
    public enum ResourceType {
        /** {@code resource}: apply Permissions changes to a {@link io.chino.api.repository.Repository Repository} */
        REPOSITORY,
        /** {@code resource}: apply Permissions changes to a {@link io.chino.api.schema.Schema Schema} */
        SCHEMA,
        /** {@code resource}: apply Permissions changes to a {@link io.chino.api.document.Document Document} */
        DOCUMENT,
        /** {@code resource}: apply Permissions changes to a {@link io.chino.api.userschema.UserSchema UserSchema} */
        USER_SCHEMA,
        /** {@code resource}: apply Permissions changes to a {@link User}'s data */
        USER,
        /** {@code resource}: apply Permissions changes to all members of {@link Group} */
        GROUP;

        @Override
        public String toString() {
            if (this == REPOSITORY)
                return "repositories";
            return this.name().toLowerCase() + "s";
        }

        public static ResourceType fromString(String string) {
            // Convert to uppercase and support both "user_schema" and "UserSchema"
            string = string.toUpperCase().replaceFirst("^USERSCHEMA", "USER_SCHEMA");
            // if 'string' is plural, it won't match, so we always try to convert to singular
            String singular = string.replace("REPOSITORIES", "REPOSITORY")
                    .replaceAll("S$", "");
            for (ResourceType type : values()) {
                // Here 'string' and 'singular' are both uppercase
                if (string.equals(type.name()) || singular.equals(type.name())) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Invalid Resource Type: " + string);
        }
    }

    /**
     * Entities that can be granted or revoked Permissions.
     */
    public enum Subject {
        /** {@code subject}: change the Permissions that a {@link User} has on the specified resource. */
        USER,
        /**
         * {@code subject}: change the Permissions that all members of {@link Group} have
         *                  on the specified resource.
         */
        GROUP;

        @Override
        public String toString() {
            return this.name().toLowerCase() + "s";
        }
    }

}
