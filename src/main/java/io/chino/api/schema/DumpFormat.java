package io.chino.api.schema;

import io.chino.api.search.SearchQueryBuilder;

import java.util.List;

/**
 * Available file formats for the Schema Dump operation:
 * <ul>
 *     <li>{@link #CSV}</li>
 *     <li>{@link #JSON}</li>
 * </ul>
 *
 * @see io.chino.java.Schemas#startSchemaDump(String, List, SearchQueryBuilder, List, DumpFormat)
 * @see io.chino.java.Schemas#startSchemaDump(String, List, SearchQueryBuilder, List, io.chino.api.common.DumpFormat)
 *
 * @deprecated in favor of {@link io.chino.api.common.DumpFormat}
 */
@Deprecated
public enum DumpFormat {
    /** Download the {@link Schema} dump in JSON format */
    CSV,
    /** Download the {@link Schema} dump in CSV format */
    JSON;

    public String toString() {
        return this.name().toLowerCase();
    }
}
