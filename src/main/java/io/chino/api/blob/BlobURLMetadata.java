package io.chino.api.blob;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.Date;

@JsonPropertyOrder({
    "token",
    "expiration",
    "one_time",
})
public class BlobURLMetadata {
    @JsonProperty("token")
    private String token;
    @JsonProperty("expiration")
    private Date expiration;
    @JsonProperty("one_time")
    private boolean oneTime;

    private BlobURLMetadata() { /* Empty constructor required for serialization */ }

    public BlobURLMetadata(String token, Date expiration, boolean oneTime) {
        this.token = token;
        this.expiration = expiration;
        this.oneTime = oneTime;
    }

    /**
     * Build a full BLOB direct URL from the metadata.
     * The method will NOT check the validity of the URL:
     * in case it has expired, or if too many people already accessed it, using the link may result in an error.
     *
     * @param baseUrl the base URL of the Chino API server where the blob is found - typically one of
     *                <b>https://api.chino.io/v1</b> or <b>https://api.test.chino.io/v1</b>
     * @param blobId the ID of the BLOB to retrieve
     * @param mimeType pass a value different from {@code null} to override the automatic inferred MIME type
     *
     * @return The direct link to the BLOB, as a String
     */
    public String getFullBlobLink(String baseUrl, String blobId, String mimeType) {
        String fullUrl = getFullBlobLink(baseUrl, blobId);
        String mimeTypeParam = String.format("&mimetype=%s", mimeType);
        return fullUrl + mimeTypeParam;
    }

    /**
     * Build a full BLOB direct URL from the metadata.
     * The method will NOT check the validity of the URL:
     * in case it has expired, or if too many people already accessed it, using the link may result in an error.
     *
     * @param baseUrl the base URL of the Chino API server where the blob is found - typically one of
     *                <b>https://api.chino.io/v1</b> or <b>https://api.test.chino.io/v1</b>
     * @param blobId the ID of the BLOB to retrieve
     *
     * @return The direct link to the BLOB, as a String
     *
     * @see BlobURLMetadata#getFullBlobLink(String, String, String) Use a custom MIME type
     */
    public String getFullBlobLink(String baseUrl, String blobId) {
        while (baseUrl.endsWith("/")) {
            // remove last character
            baseUrl = baseUrl.substring(0, baseUrl.length() - 1);
        }
        // Compute API link
        return String.format("%s/blobs/url/%s?token=%s", baseUrl, blobId, this.token);
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setOneTime(boolean oneTime) {
        this.oneTime = oneTime;
    }

    public boolean isOneTime() {
        return oneTime;
    }
}
