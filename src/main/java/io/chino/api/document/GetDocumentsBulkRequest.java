package io.chino.api.document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ids"
})
public class GetDocumentsBulkRequest {

    @JsonProperty("ids")
    private List<String> documentIds = new ArrayList<>();


    public GetDocumentsBulkRequest(List<String> documentIds) {
        this.documentIds = documentIds;
    }


    /**
     * @return document IDs
     */
    public List<String> getDocumentIds() {
        return documentIds;
    }

    /**
     * @param documentIds the document IDs
     */
    public void setDocumentIds(List<String> documentIds) {
        this.documentIds = documentIds;
    }

    @Override
    public String toString() {
        return "GetDocumentsBulkRequest{" +
                "documentIds=" + documentIds +
                '}';
    }
}
