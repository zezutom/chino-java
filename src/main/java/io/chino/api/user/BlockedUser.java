package io.chino.api.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contains information about a blocked ({@link User}, IP address) pair.
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({
    "user_id",
    "username",
    "ip",
    "block_ttl",
})
public class BlockedUser {
    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("username")
    private String username;

    @JsonProperty("ip")
    private String ipAddr;

    @JsonProperty("block_ttl")
    private Integer blockTimeToLive;


    public BlockedUser() { }


    /**
     * 
     * @return the user_id
     */
    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId the user_id
     */
    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * 
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 
     * @return the IP address blocked
     */
    public String getIpAddr() {
        return ipAddr;
    }

    /**
     * 
     * @param ipAddr the IP address blocked
     */
    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    /**
     * 
     * @return the block's TTL in seconds
     */
    public Integer getBlockTimeToLive() {
        return blockTimeToLive;
    }

    /**
     * 
     * @param blockTimeToLive the block's TTL in seconds
     */
    public void setBlockTimeToLive(Integer blockTimeToLive) {
        this.blockTimeToLive = blockTimeToLive;
    }
}
