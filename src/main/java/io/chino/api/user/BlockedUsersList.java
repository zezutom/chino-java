package io.chino.api.user;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Contains the {@code user_id} of a blocked {@link User}.
 * 
 */
public class BlockedUsersList extends LinkedList<BlockedUser> {

    /**
     * Get the {@link User#userId user IDs} of all the blocked Users contained in this {@link BlockedUsersList}.
     * 
     * @return a {@link List} of UUIDs of the blocked users.
     */
    public List<String> getUserIds() {
        LinkedList<String> userIds = new LinkedList<>();
        this.forEach(blockedUser -> {
            userIds.add(blockedUser.getUserId());
        });

        return userIds;
    }
}
