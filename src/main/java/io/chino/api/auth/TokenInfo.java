package io.chino.api.auth;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

/**
 * Information about the auth token returned by the OAuth Introspect endpoint
 */
@JsonInclude(JsonInclude.Include.ALWAYS)
@JsonPropertyOrder({
        "active",
        "scope",
        "exp",
        "client_id",
        "username"
})
public class TokenInfo {

    @JsonProperty("active")
    private boolean active;
    @JsonProperty("scope")
    private String scope;
    @JsonProperty("exp")
    private int exp;
    @JsonProperty("client_id")
    private String clientId;
    @JsonProperty("username")
    private String username;

    @JsonProperty("active")
    public boolean isActive() {
        return active;
    }

    @JsonProperty("scope")
    public String getScope() {
        return scope;
    }

    @JsonProperty("exp")
    public int getExp() {
        return exp;
    }

    @JsonProperty("client_id")
    public String getClientId() {
        return clientId;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TokenInfo.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("active");
        sb.append('=');
        sb.append(this.active);
        sb.append(',');
        sb.append("scope");
        sb.append('=');
        sb.append(((this.scope == null)?"<null>":this.scope));
        sb.append(',');
        sb.append("exp");
        sb.append('=');
        sb.append(this.exp);
        sb.append(',');
        sb.append("clientId");
        sb.append('=');
        sb.append(((this.clientId == null)?"<null>":this.clientId));
        sb.append(',');
        sb.append("username");
        sb.append('=');
        sb.append(((this.username == null)?"<null>":this.username));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+(this.active? 1 : 0));
        result = ((result* 31)+((this.clientId == null)? 0 :this.clientId.hashCode()));
        result = ((result* 31)+ this.exp);
        result = ((result* 31)+((this.scope == null)? 0 :this.scope.hashCode()));
        result = ((result* 31)+((this.username == null)? 0 :this.username.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) return true;

        if ( ! (other instanceof TokenInfo) ) return false;

        TokenInfo otherToken = ((TokenInfo) other);
        return this.active == otherToken.active &&
                Objects.equals(this.clientId, otherToken.clientId) &&
                this.exp == otherToken.exp &&
                Objects.equals(this.scope, otherToken.scope) &&
                Objects.equals(this.username, otherToken.username);
    }

}
