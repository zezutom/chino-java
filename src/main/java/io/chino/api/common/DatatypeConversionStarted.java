package io.chino.api.common;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Wraps the message returned by the Chino.io API when starting an operation of Datatype Conversion
 * on a {@link io.chino.api.schema.Schema Schema} or {@link io.chino.api.userschema.UserSchema UserSchema}
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "message"
})
public class DatatypeConversionStarted {

    @JsonProperty("message")
    private String message;

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

}
