package io.chino.api.common;

import java.util.LinkedHashMap;
import java.util.stream.Collectors;

/**
 * Base class to implement URL query filtering.<br>
 * <br>
 * Subclasses must validate the filters, convert them to String and <b>ensure they are URL-safe</b>.
 * Then they can add the filters by calling {@link #addFilter(String, String)}.
 */
public abstract class UrlFilters {
    // NOTE: for rendering arrays, we may need to allow duplicate keys in the URL.
    //       Better store the array/collection in the 'filters' HashMap though,
    //       and then render the array as multiple identical keys when parsing the value in `toString()`.
    private final LinkedHashMap<String, String> filters = new LinkedHashMap<>();

    /**
     * Add a filter as a &lt;name, value&gt; pair
     *
     * @param name URL-safe name of the filter
     * @param value value of the filter, properly converted to a URL-safe {@link String}
     */
    protected void addFilter(String name, String value) {
        filters.put(name, value);
    }

    /**
     * Clear all the existing filters
     */
    public void clear() {
        filters.clear();
    }

    /**
     * Convert the filters to a URL query.<br>
     * <br>
     *
     * The returned {@link String} will be empty if no filters have been added yet, otherwise it will contain the
     * {@code key=value} separated by the {@code '&'} character.<br>
     *
     * <br>
     * <b>Caveats:</b>
     * <ul>
     *     <li>No validation is done on the parameters, they must all be validated and URL-safe.</li>
     *     <li>No {@code '?'} character is added at the beginning of the String.</li>
     * </ul>
     *
     * @return a URL query for the current status of this {@link UrlFilters}.
     */
    @Override
    public String toString() {
        return filters.keySet().stream()
                .map(key -> key + "=" + filters.get(key))
                .collect(Collectors.joining("&"));
    }
}
