package io.chino.api.common;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestDetailsInterceptor implements Interceptor {

    private boolean sanitize = true;

    public RequestDetailsInterceptor(boolean sanitizeHeaders) {
        this.sanitize = sanitizeHeaders;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (chain == null) return null;  // skip
        Request request = chain.request();

        // sanitize headers
        Headers headers = request.headers();
        HashMap<String, String> outHeaders = new HashMap<>();
        headers.forEach(h -> {
            String name = h.getFirst(), value = h.getSecond();
            if (this.sanitize && name.toLowerCase().equals("authorization")) {
                String[] tokens = value.split(" ");
                value = tokens[0] + " " + "<sanitized>";
            }
            outHeaders.put(name, value);
        });

        // Print debug information (test only)
        System.out.println("*** REQUEST");
        System.out.println(request.method() + " " + request.url());
        System.out.println("*** HEADERS");
        // print headers
        outHeaders.forEach((k, v) -> System.out.println(k + ": " + v));
        System.out.println();

        return chain.proceed(request);
    }

}
