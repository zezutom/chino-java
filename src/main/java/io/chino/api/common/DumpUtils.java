package io.chino.api.common;

import okhttp3.Response;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

/**
 * Utility class for the dump operations on {@link io.chino.api.schema.Schema Schemas} and
 * {@link io.chino.api.userschema.UserSchema UserSchemas}
 */
public class DumpUtils {

    /**
     * Writes the content of a HTTP response stream to file
     *
     * @param apiResponse {@link Response} returned by the GET dump API call
     * @param folderPath path to the folder where the dump file must be saved
     * @param fileName name to assign to the dump file. Can be {@code null}, in which case a default name will be
     *                 assigned, containing the {@code dumpId}.
     *
     * @return a {@link File} object pointing to the downloaded dump file
     *
     * @throws IOException
     */
    public static File writeDump(Response apiResponse, String folderPath, String fileName) throws IOException {
        checkNotNull(apiResponse, "apiResponse");
        checkNotNull(folderPath, "folderPath");
        checkNotNull(fileName, "fileName");

        try {
            // read bytes from response and write to file
            InputStream returnStream = apiResponse.body().byteStream();

            String outpuPath = folderPath + File.separator + fileName;
            File outputFile = new File(outpuPath);
            outputFile.getParentFile().mkdirs();
            Files.copy(returnStream, outputFile.toPath());

            return outputFile;
        } finally { // close response body to prevent warnings from OkHttp3
            apiResponse.body().close();
        }
    }

    /**
     * Verify that the {@link Object} is not a {@code null} reference; otherwise
     * interrupts execution with an exception, specifying the name of the {@code null} Object.
     *
     * @param object an {@link Object}
     * @param name a name that identifies the Object for the dev
     *
     * @throws NullPointerException the object (whose name is reported in the Exception) is {@code null}.
     */
    private static void checkNotNull(Object object, String name) {
        if (object == null) {
            throw new NullPointerException(name);
        }
    }
}
