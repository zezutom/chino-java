package io.chino.api.common;

/** placeholder class */
public final class BuildConfig
{
   private BuildConfig () {}

   // The actual value will be set at compile time when building with Gradle
   public static final String VERSION = "(not-set)";

   public static final String NAME = "chino-java";

   public static final long BUILD_UNIXTIME = 0L;
}
