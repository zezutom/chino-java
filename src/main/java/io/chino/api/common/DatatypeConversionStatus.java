package io.chino.api.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.LinkedList;
import java.util.List;


/**
 * Represents the status of an operation of Datatype Conversion on a
 * {@link io.chino.api.schema.Schema Schema} or {@link io.chino.api.userschema.UserSchema UserSchema}
 * 
 * @see io.chino.java.Schemas#getDatatypeConversionStatus(String)
 * @see io.chino.java.UserSchemas#getDatatypeConversionStatus(String)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "status",
        "percentage",
        "start",
        "end",
        "msgs"
})
public class DatatypeConversionStatus {

    @JsonProperty("status")
    private ConversionStatus status;
    @JsonProperty("percentage")
    private Integer percentage;
    @JsonProperty("start")
    private String start;
    @JsonProperty("end")
    private String end;
    @JsonProperty("msgs")
    private List<String> msgs = new LinkedList<>();

    @JsonProperty("status")
    public ConversionStatus getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(ConversionStatus status) {
        this.status = status;
    }

    @JsonProperty("percentage")
    public Integer getPercentage() {
        return percentage;
    }

    @JsonProperty("percentage")
    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    @JsonProperty("start")
    public String getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(String start) {
        this.start = start;
    }

    @JsonProperty("end")
    public String getEnd() {
        return end;
    }

    @JsonProperty("end")
    public void setEnd(String end) {
        this.end = end;
    }

    @JsonProperty("msgs")
    public List<String> getMsgs() {
        return msgs;
    }

    @JsonProperty("msgs")
    public void setMsgs(List<String> msgs) {
        if (msgs == null)
            this.msgs = new LinkedList<>();
        else
            this.msgs = new LinkedList<>(msgs);
    }
}
