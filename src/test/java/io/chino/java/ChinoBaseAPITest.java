package io.chino.java;

import io.chino.api.common.ChinoApiException;
import io.chino.api.common.Field;
import io.chino.api.common.indexed;
import io.chino.java.testutils.AllSchemaFieldTypes;
import io.chino.java.testutils.TestConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class ChinoBaseAPITest {

    private static ChinoAPI dummyClient;

    @BeforeClass
    public static void setupClass() {
        dummyClient = new ChinoAPI("");
    }

    @Test
    public void testSchemaSerialization() throws ChinoApiException {
        ChinoBaseAPI test = new ChinoBaseAPI(TestConstants.HOST, dummyClient);

        Class theClass = AllSchemaFieldTypes.class;
        List<Field> chinoFields = test.returnFields(theClass);
        java.lang.reflect.Field[] javaFieldsArray = theClass.getDeclaredFields();

        // allow chino fields to be retrieved by name
        HashMap<String, Field> fieldsByName = new HashMap<>();
        for (Field chinoField : chinoFields) {
            fieldsByName.put(chinoField.getName(), chinoField);
        }
        // remove transient fields
        LinkedList<java.lang.reflect.Field> javaFields = new LinkedList<>();
        for (java.lang.reflect.Field javaField : javaFieldsArray) {
            if (! Modifier.isTransient(javaField.getModifiers())) {
                javaFields.add(javaField);
            }
        }

        if (chinoFields.size() != javaFields.size()) {
            System.err.println("Java field <-> Chino field");
            System.err.println("- - - - - - - - - - - - - - - - - - ");
            for (java.lang.reflect.Field javaField : javaFields) {
                String javaFieldStr = String.format("%s (%s)", javaField.getName(), javaField.getType().getSimpleName());
                Field chinoField = fieldsByName.getOrDefault(javaField.getName(), new Field("null", "null"));
                String chinoFieldStr = String.format("%s (%s)", chinoField.getName(), chinoField.getType());

                System.err.printf("%s <-> %s\n", javaFieldStr, chinoFieldStr);
                System.err.flush();
            }
            fail("Not all Java fields have been converted");
        }

        for (java.lang.reflect.Field javaField : javaFields) {
            String name = javaField.getName();

            // check field "name"
            Field chinoField = fieldsByName.getOrDefault(name, null);
            assertNotNull(name, chinoField);

            // Check field "type"
            Class javaType = javaField.getType();
            assertEquals(name, test.getChinoType(javaType), chinoField.getType());

            // check "indexed" status
            if (javaField.getAnnotation(indexed.class) != null) {
                assertTrue(name, chinoField.getIndexed());
            } else {
                assertTrue(chinoField.getIndexed() == null || chinoField.getIndexed() == false);
            }

            // TODO: add tests for "default" and "insensitive"
        }
    }
}
