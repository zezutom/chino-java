package io.chino.java;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.application.Application;
import io.chino.api.application.ClientType;
import io.chino.api.common.ChinoApiException;
import io.chino.api.document.Document;
import io.chino.api.document.GetDocumentsBulkResponse;
import io.chino.api.user.*;
import io.chino.api.userschema.UserSchema;
import io.chino.java.testutils.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class UsersTest extends ChinoBaseTest {

    private static final String PWD = "defaultPwd";
    private static final int MAX_USERS = 10;
    private static ChinoAPI chino_admin;
    private static Users test;
    private static UserSchema userSchema;
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");

    @BeforeClass
    public static void setUpClass() throws Exception {
        ChinoBaseTest.runClass(UsersTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        test = ChinoBaseTest.init(chino_admin.users);

        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.userSchemas.list().getUserSchemas().isEmpty(),
                chino_admin.userSchemas
        );

        try {
            userSchema = chino_admin.userSchemas.create("Test UserSchema for class UsersTest"  + " [" + TestConstants.JAVA + "]", UserSchemaStructureSample.class);
        } catch (Exception ex) {
            fail("failed to set up test for UsersTest (create UserSchema).\n"
                    + ex.getClass().getSimpleName() + ": " + ex.getMessage());
        }
    }

    @Before
    public void setUp() throws IOException, ChinoApiException {
        new DeleteAll().deleteAll(test);
    }

    @Test
    public void checkPasswordTest() throws IOException, ChinoApiException {
        User u = newUser("checkPasswordTest");

        try {
            // try to login without authenticating
            test.checkPassword(TestConstants.PASSWORD);
        } catch (ChinoApiException ex) {
            assertEquals("403", ex.getCode());
            assertTrue(ex.getMessage().contains("Only User can get user info"));
        }

        // verify that login is enabled with new credentials
        Application app = chino_admin.applications.create(
                "test Application for checkPasswordTest" + "[" + TestConstants.JAVA + "]",
                "password",
                "",
                ClientType.PUBLIC
        );
        ChinoAPI client = null;
        String token = null;
        try {
            client = new ChinoAPI(TestConstants.HOST);
            token = client.auth.loginWithPassword(u.getUsername(), TestConstants.PASSWORD, app.getAppId())
                    .getAccessToken();

            assertFalse("Returned TRUE with wrong password",
                    client.users.checkPassword("wrong password!")
            );
            assertTrue("Returned FALSE with correct password",
                    client.users.checkPassword(TestConstants.PASSWORD)
            );
        } finally {
            if (client != null && token != null)
                client.auth.logout(token, app.getAppId());
            chino_admin.applications.delete(app.getAppId());
        }
    }

    @Test
    public void listTest() throws IOException, ChinoApiException {
        LinkedList<User> localUsers = new LinkedList<>();
        for (int i = 0; i < MAX_USERS; i++) {
                localUsers.add(
                        newUser("listTest_" + i)
                );
        }
        /* LIST 1 params */
        LinkedList<User> fetchedUsers = new LinkedList<>(
                test.list(userSchema.getUserSchemaId()).getUsers()
        );
        // check that MAX_USERS users are retrieved
        assertEquals("unexpected size of the fetched Users list", MAX_USERS, fetchedUsers.size());

        int missing = 0;
        StringBuilder missingUsers = new StringBuilder();
        for (User u : localUsers) {
            if (! fetchedUsers.contains(u)) {
                missing ++;
                missingUsers.append("\t" + (++ missing) + ". ").append(u.getUsername()).append("\n");
            }
        }
        // check that all created users are retrieved
        assertEquals(missing + " Users missing: \n" + missingUsers.toString(), 0, missing);

        /* LIST 3 params - offset */
        int offset = MAX_USERS / 2;
        fetchedUsers = new LinkedList<>(
                test.list(userSchema.getUserSchemaId(), offset, MAX_USERS).getUsers()
        );
        // check that (MAX_USERS - offset) users are retrieved
        assertEquals("unexpected size of the fetched Users list (OFFSET = " + offset + ")", MAX_USERS - offset, fetchedUsers.size());

        missing = 0;
        missingUsers = new StringBuilder();
        for (User u : fetchedUsers) {
            if (! localUsers.contains(u)) {
                missing ++;
                missingUsers.append("\t" + (++ missing) + ". ").append(u.getUsername()).append("\n");
            }
        }
        // check that all fetched users are in the local list
        assertEquals(missing + " Users missing: \n" + missingUsers.toString(), 0, missing);

        /* LIST 3 params - limit */
        int limit = 1;
        fetchedUsers = new LinkedList<>(
                test.list(userSchema.getUserSchemaId(), MAX_USERS - 1, limit).getUsers()
        );
        // check that 1 user was retrieved and that it's in the local list
        assertEquals("unexpected size of the fetched Users list (LIMIT = " + limit + ")", 1, fetchedUsers.size());
        assertTrue(localUsers.contains(fetchedUsers.get(0)));
    }

    @Test
    public void testBulkGet() throws IOException, ChinoApiException {
        // Create 3 users
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            User user = newUser("test"+i);
            users.add(user);
        }

        List<String> userIds = users.stream().map(User::getUserId).collect(Collectors.toList());
        GetUsersBulkResponse response = test.bulkGet(userIds);

        assertNotNull(response);
        assertEquals(3, response.getCount().intValue());
        assertEquals(3, response.getTotalCount().intValue());
        assertEquals(0, response.getErrors().size());

        for (Map.Entry<String, User> entry : response.getUsers().entrySet()) {
            String userId = entry.getKey();
            User user = entry.getValue();

            assertTrue(userIds.contains(userId));
            assertEquals(userId, user.getUserId());
        }

        // Delete all users in the end
        for (User user : users) {
            test.delete(user.getUserId(), true);
        }

    }

    @Test
    public void createSyncAndReadTest() throws IOException, ChinoApiException {
        UserSchemaStructureSample attr = new UserSchemaStructureSample("readTest");
        attr.test_integer = 42;
        /* CREATE - attrs passed as JSON String; synchronous call (consistent = true) */
        User local = newUser("createAndReadTest_1 (attributes as String)", attr, true);

        /* READ 1 params */
        User fetched = test.read(local.getUserId());
        assertEquals("Read user is different from local.", local, fetched);

        /* CREATE - attrs passed as HashMap */
        String testName = "test String - createAndReadTest_2 (attributes as HashMap)";
        HashMap<String, Object> attrMap = new HashMap<>();
        attrMap.put("test_integer", 42);
        attrMap.put("test_boolean", true);
        attrMap.put("test_float", (float) 0.987);
        attrMap.put("test_string", testName);

        attrMap.put("test_date", dateFormat.format(new Date()));

        local = test.create(  "createAndReadTest_2_user@test.chino.io", "defaultPwd", attrMap, userSchema.getUserSchemaId());

        /* READ 2 params */
        UserSchemaStructureSample fetchedAttr = (UserSchemaStructureSample) test.read(local.getUserId(), UserSchemaStructureSample.class);

        assertEquals("Read user has different int attribute from local", attr.test_integer, fetchedAttr.test_integer);
        assertEquals("Read user has different String attribute from local", testName, fetchedAttr.test_string);
    }

    @Test
    public void updatePartialSyncTest_HashMap() throws IOException, ChinoApiException {
        User old = newUser("updatePartialSyncTest_HashMap");

        String newUsername = null,
                newPassword = null;

        for (boolean consistent : new boolean[] {true, false}) {
            UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(old.getUserId(), UserSchemaStructureSample.class);
            UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
            newAttrs.test_integer ++;

            newPassword = "updatedPW";
            newUsername = (consistent) ? "SYNC" : "ASYNC" + old.getUsername();

            HashMap<String, Object> updatedAttributes = new HashMap<>();
            updatedAttributes.put("test_integer", newAttrs.test_integer);

            updatedAttributes.put("username", newUsername);
            updatedAttributes.put("password", newPassword);

            User updated;
            updated = consistent ? test.updatePartial(old.getUserId(), updatedAttributes, true)
                    : test.updatePartial(old.getUserId(), updatedAttributes);

            assertEquals(old.getUserId(), updated.getUserId());
            assertEquals("user not updated!", newAttrs.test_integer, updated.getAttributesAsHashMap().get("test_integer"));
            assertNotEquals("user not updated!", old.getUsername(), updated.getUsername());

            old = updated;
        }

        // verify that login is enabled with new credentials
        Application app = chino_admin.applications.create("test Application for UsersTest" + "[" + TestConstants.JAVA + "]", "password", "", ClientType.PUBLIC);
        try {
            ChinoAPI appClient = new ChinoAPI(TestConstants.HOST);
            appClient.setBearerToken(
                    appClient.auth.loginWithPassword(newUsername, newPassword, app.getAppId()).getAccessToken()
            );
        } finally {
            chino_admin.applications.delete(app.getAppId());
        }
    }

    @Test
    public void updatePartialTest_checkReuse() throws IOException, ChinoApiException {
        User old = newUser("updatePartialTest_checkReuse");

        // Last one should return 400 because we're using the same password again.
        String[] passwords = {"password1", "password2", "password3"};

        for (String password: passwords) {
            UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(
                    old.getUserId(), UserSchemaStructureSample.class
            );
            UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
            newAttrs.test_integer++;

            HashMap<String, Object> updatedAttributes = new HashMap<>();
            updatedAttributes.put("test_integer", newAttrs.test_integer);
            updatedAttributes.put("password", password);

            User updated = test.updatePartial(false, old.getUserId(), updatedAttributes, true, true);
            // check user was updated
            assertEquals(old.getUserId(), updated.getUserId());
            assertEquals("user not updated!", newAttrs.test_integer, updated.getAttributesAsHashMap().get("test_integer"));

            old = updated;
        }

        // Try to reuse first password, expect 400
        String duplicatedPsw = "password1";

        UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(
                old.getUserId(), UserSchemaStructureSample.class
        );
        UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
        newAttrs.test_integer++;

        HashMap<String, Object> updatedAttributes = new HashMap<>();
        updatedAttributes.put("test_integer", newAttrs.test_integer);

        updatedAttributes.put("password", duplicatedPsw);

        try {
            test.updatePartial(false, old.getUserId(), updatedAttributes, true, true);
            fail("Expected a 400 error");
        } catch (ChinoApiException e) {
            assertEquals("Wrong exception - " + e.getMessage(), "400", e.getCode());
        }
    }

    @Test
    public void updatePartialTest_String() throws IOException, ChinoApiException {
        User old = newUser("updatePartialTest_String");

        for (boolean consistent : new boolean[] {true, false}) {
            UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(old.getUserId(), UserSchemaStructureSample.class);
            UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
            newAttrs.test_integer ++;

            String updatedAttributes = "{" +
                    "  \"test_integer\":" + newAttrs.test_integer +
                    "}";

            User updated;
            updated = consistent ? test.updatePartial(old.getUserId(), updatedAttributes, true)
                        : test.updatePartial(old.getUserId(), updatedAttributes);

            assertNotNull("user not updated!", updated);
            assertEquals("user not updated!", newAttrs.test_integer, updated.getAttributesAsHashMap().get("test_integer"));
            old = updated;
        }
    }

    @Test
    public void updateTestSync_HashMap() throws IOException, ChinoApiException {
        User old = newUser("updateTestSync_HashMap");

        for (boolean consistent : new boolean[] {true, false}) {
            UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(old.getUserId(), UserSchemaStructureSample.class);

            HashMap<String, Object> updatedAttributes = new HashMap<>();
            updatedAttributes.put("test_integer", ++ oldAttrs.test_integer);
            updatedAttributes.put("test_boolean", oldAttrs.test_boolean);
            updatedAttributes.put("test_date", dateFormat.format(oldAttrs.test_date));
            updatedAttributes.put("test_float", oldAttrs.test_float);
            updatedAttributes.put("test_string", "updateTest_HashMap_updated");

            User updated;
            updated = consistent ? test.update(old.getUserId(), old.getUsername(), TestConstants.PASSWORD, updatedAttributes, true)
                    : test.update(old.getUserId(), old.getUsername(), TestConstants.PASSWORD, updatedAttributes);

            assertNotNull("user not updated!", updated);
            assertEquals("user not updated!", oldAttrs.test_integer, updated.getAttributesAsHashMap().get("test_integer"));
            old = updated;
        }
    }

    @Test
    public void updateTestSync_checkReuse() throws IOException, ChinoApiException {
        User old = newUser("updateTestSync_checkReuse");

        // Last one should return 400 because we're using the same password again.
        String[] passwords = {"password1", "password2", "password3"};

        for (String password : passwords) {
            UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(
                    old.getUserId(), UserSchemaStructureSample.class
            );
            UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
            newAttrs.test_integer++;

            HashMap<String, Object> updatedAttributes = new HashMap<>();
            updatedAttributes.put("test_integer", newAttrs.test_integer);
            updatedAttributes.put("test_boolean", oldAttrs.test_boolean);
            updatedAttributes.put("test_date", dateFormat.format(oldAttrs.test_date));
            updatedAttributes.put("test_float", oldAttrs.test_float);
            updatedAttributes.put("test_string", "updateTest_HashMap_updated");

            User updated = test.update(false, old.getUserId(), old.getUsername(), password, updatedAttributes, true, true);
            // check user was updated
            assertEquals(old.getUserId(), updated.getUserId());
            assertEquals("user not updated!", newAttrs.test_integer, updated.getAttributesAsHashMap().get("test_integer"));

            old = updated;
        }

        // Try to reuse first password, expect 400
        String duplicatedPsw = "password1";

        UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(
                old.getUserId(), UserSchemaStructureSample.class
        );
        UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
        newAttrs.test_integer++;

        HashMap<String, Object> updatedAttributes = new HashMap<>();
        updatedAttributes.put("test_integer", newAttrs.test_integer);

        updatedAttributes.put("password", duplicatedPsw);

        try {
            test.update(false, old.getUserId(), old.getUsername(), duplicatedPsw, updatedAttributes, true, true);
            fail("Expected a 400 error");
        } catch (ChinoApiException e) {
            assertEquals("Wrong exception - " + e.getMessage(), "400", e.getCode());
        }
    }

    @Test
    public void updateTest_String() throws IOException, ChinoApiException {
        User old = newUser("updateTest_String");
        for (boolean consistent : new boolean[] {true, false}) {
            UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(old.getUserId(), UserSchemaStructureSample.class);
            UserSchemaStructureSample newAttrs = new UserSchemaStructureSample(oldAttrs);
            newAttrs.test_integer++;

            String updatedAttributes = new ObjectMapper().writeValueAsString(newAttrs);

            User updated;
            updated = consistent ? test.update(old.getUserId(), old.getUsername(), TestConstants.PASSWORD, updatedAttributes, true)
                    :test.update(old.getUserId(), old.getUsername(), TestConstants.PASSWORD, updatedAttributes);

            assertNotNull("user not updated!", updated);
            assertEquals("user not updated!", newAttrs.test_integer, updated.getAttributesAsHashMap().get("test_integer"));
            old = updated;
        }
    }

    @Test
    public void activateUserTest_update_updatePartial() throws IOException, ChinoApiException {
        User user0 = newUser("activateUserTest_update_updatePartial0");
        User user1 = newUser("activateUserTest_update_updatePartial1");
        User user2 = newUser("activateUserTest_update_updatePartial2");
        int iteration = -1;
        for (User usr : new User[] {user0, user1, user2}) {
            String id = usr.getUserId();
            // Set is_active = false
            test.delete(id, false);
            assertFalse("Failed to set inactive", test.read(id).getIsActive());
            // Set is_active = true with different methods
            doActivation(++iteration, id, usr.getUsername(), TestConstants.PASSWORD);
            User control = test.read(id);
            // Verify update
            assertTrue("Failed to activate", control.getIsActive());
            assertNotEquals("Failed to update string attribute after activation",
                    usr.getAttributesAsHashMap().get("test_string"),
                    control.getAttributesAsHashMap().get("test_string")
            );
            assertEquals("Failed to update integer attribute after activation",
                    iteration,
                    control.getAttributesAsHashMap().get("test_integer")
            );

            test.delete(id, true);
        }
    }

    @Test
    public void blockedUsersTest() throws IOException, ChinoApiException, InterruptedException {
        User user = newUser("blockedUsersTest");
        Application app = chino_admin.applications.create("test Application for UsersTest" + "[" + TestConstants.JAVA + "]", "password", "", ClientType.PUBLIC);
        BlockedUsersList res = null;
        
        // Check user can login with correct credentials.
        // Use a different client to avoid overwriting the client's Auth
        ChinoAPI loginClient = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        loginClient.auth.loginWithPassword(user.getUsername(), TestConstants.PASSWORD, app.getAppId());
        
        // Login with a wrong password to get the user blocked
        for (int i = 0; i < 5; i++) {
            try {
                loginClient.auth.loginWithPassword(user.getUsername(), "wrong_password", app.getAppId());
            } catch (ChinoApiException e) {
                // ignore the failed login
            }
            TimeUnit.SECONDS.sleep(1);
        }

        // Test the call to listBlockedUserIds without params
        res = chino_admin.users.listBlockedUserIds();
        // Make sure that the user exists in the list of blocked Users
        BlockedUser bu = null;
        for (BlockedUser blockedUser : res) {
            if (blockedUser.getUserId().equals(user.getUserId())) {
                bu = blockedUser;
            }
        }
        // res.forEach(blockedUser -> {
        //     if (blockedUser.getUserId().equals(user.getUserId())) {
        //         bu = blockedUser;
        //     }
        // });
        assertNotNull("User " + user.getUserId() + " is not Blocked", bu);
        // Test class methods of BlockedUser
        assertNotNull("null: user_id", bu.getUserId());
        assertNotNull("null: username", bu.getUsername());
        assertNotNull("null: ip", bu.getIpAddr());
        assertNotNull("null: block_ttl", bu.getBlockTimeToLive());

        // Test the call to listBlockedUserIds with params
        res = chino_admin.users.listBlockedUserIds(user.getUserId(), bu.getIpAddr());
        // Make sure that the user exists in the list of blocked Users
        assertTrue("User " + user.getUserId() + " is not in the list with filters",
            res.getUserIds().contains(user.getUserId()));

        // Unblock User
        chino_admin.users.unblockUser(bu.getUserId(), bu.getIpAddr());
        TimeUnit.SECONDS.sleep(2);

        // Check that the user was unblocked
        res = chino_admin.users.listBlockedUserIds(user.getUserId(), bu.getIpAddr());
        // Make sure that the user exists in the list of blocked Users
        assertFalse("User " + user.getUserId() + " was not unblocked",
            res.getUserIds().contains(bu.getUserId()));
        
    }

    private void doActivation(int methodId, String userId, String username, String password) throws IOException, ChinoApiException {
        UserSchemaStructureSample oldAttrs = (UserSchemaStructureSample) test.read(userId, UserSchemaStructureSample.class);
        HashMap<String, Object> updatedAttributes = new HashMap<>();
        updatedAttributes.put("test_string", "activateUserTest_update_updatePartial:updated");
        updatedAttributes.put("test_integer", methodId);
        switch (methodId) {
            case 0:
                test.updatePartial(true, userId,
                        updatedAttributes,
                        true
                );
                break;
            case 1:
                updatedAttributes.put("test_boolean", oldAttrs.test_boolean);
                updatedAttributes.put("test_date", dateFormat.format(oldAttrs.test_date));
                updatedAttributes.put("test_float", oldAttrs.test_float);
                test.update(true, userId, username, password,
                        updatedAttributes,
                        true
                );
                break;
            case 2:
                updatedAttributes.put("test_boolean", oldAttrs.test_boolean);
                updatedAttributes.put("test_date", dateFormat.format(oldAttrs.test_date));
                updatedAttributes.put("test_float", oldAttrs.test_float);
                test.update(true, userId, username, password,
                        new ObjectMapper().writeValueAsString(updatedAttributes),
                        true
                );
                break;
            default:
                throw new IllegalArgumentException("Invalid iteration index: " + methodId);
        }
    }

    @Test
    public void deleteTest() throws IOException, ChinoApiException {
        User user = newUser("deleteTest");

        test.delete(user.getUserId(), false);

        user = test.read(user.getUserId());
        assertFalse("User is active", user.getIsActive());

        test.delete(user.getUserId(), true);

        try {
            user = test.read(user.getUserId());
            fail("The user was not deleted");
        } catch (ChinoApiException e) {
            assertEquals("Unexpected error when attempting to read deleted user: " + e.getCode(), "404", e.getCode());
        }
    }

    /**
     * Utility method to create a new User with custom attributes
     *
     */
    private User newUser(String testName, UserSchemaStructureSample customAttrs, boolean consistent) throws IOException, ChinoApiException {
        String attrs = new ObjectMapper().writeValueAsString(customAttrs);
        String username = testName.trim().split(" ")[0];
        if (consistent)
            return test.create(username + "_user@test.chino.io", TestConstants.PASSWORD, attrs, userSchema.getUserSchemaId(), consistent);
        else
            return test.create(username + "_user@test.chino.io", TestConstants.PASSWORD, attrs, userSchema.getUserSchemaId());
    }

    /**
     * Utility method to create a new User with default attributes
     *
     */
    private User newUser(String testName) throws IOException, ChinoApiException {
        UserSchemaStructureSample userAttributes = new UserSchemaStructureSample(testName);

        return newUser(testName, userAttributes, false);
    }
}