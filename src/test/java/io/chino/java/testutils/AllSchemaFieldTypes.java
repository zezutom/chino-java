package io.chino.java.testutils;


import com.fasterxml.jackson.databind.JsonNode;
import io.chino.api.common.indexed;

import java.io.File;
import java.sql.Time;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class AllSchemaFieldTypes {
    // string
    String string;
    @indexed
    String stringIndexed;
    // array[string]
    String[] stringArray;
    @indexed String[] stringArrayIndexed;

    // integer
    int integer1;
    @indexed int integer1Indexed;
    Integer integer2;
    @indexed Integer integer2Indexed;
    // array[integer]
    int[] integerArray1;
    @indexed int[] integerArray1Indexed;
    Integer[] integerArray2;
    @indexed Integer[] integerArray2Indexed;

    // float
    float float1;
    @indexed float float1Indexed;
    Float float2;
    @indexed Float float2Indexed;
    // array[float]
    float[] floatArray1;
    @indexed float[] floatArray1Indexed;
    Float[] floatArray2;
    @indexed Float[] floatArray2Indexed;

    // boolean
    boolean boolean1;
    @indexed boolean boolean1Indexed;
    Boolean boolean2;
    @indexed Boolean boolean2Indexed;

    // date
    java.util.Date date1;
    @indexed java.util.Date date1Indexed;
    java.sql.Date date2;
    @indexed java.sql.Date date2Indexed;

    // time
    Time time;
    @indexed Time timeIndexed;

    // datetime
    java.sql.Timestamp datetime1;
    @indexed java.sql.Timestamp datetime1Indexed;
    java.security.Timestamp datetime2;
    @indexed java.security.Timestamp datetime2Indexed;

    // blob
    File blob;
    @indexed File blobIndexed;

    // base64
    Base64 base64;
    @indexed Base64 base64Indexed;

    // json
    HashMap json1;
    @indexed HashMap json1Indexed;
    LinkedHashMap json2;
    @indexed LinkedHashMap json2Indexed;
    JsonNode json3;
    @indexed JsonNode json3Indexed;
}