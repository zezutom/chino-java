package io.chino.java;

import com.fasterxml.jackson.databind.JsonMappingException;
import io.chino.api.blob.BlobURLMetadata;
import io.chino.api.blob.CommitBlobUploadResponse;
import io.chino.api.blob.GetBlobResponse;
import io.chino.api.common.ChinoApiException;
import io.chino.api.common.Field;
import io.chino.api.document.Document;
import io.chino.api.repository.Repository;
import io.chino.api.schema.SchemaStructure;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.TestConstants;
import org.junit.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class BlobsTest extends ChinoBaseTest {

    private static ChinoAPI chino_admin, chino_noauth;
    private static Blobs test;

    private static String REPO_ID, SCHEMA_ID;
    private static String blobFieldName = "the_blob";
    private static String blobFileName = "chino_logo.jpg",
            resFolder = "src/test/res/".replace("/",  File.separator);
    private static String outputPath = resFolder + "control",
            linkOutputPath = outputPath + File.separator + "link";
    private static String blobId;

    private static final LinkedList<String> outputFiles = new LinkedList<>();

    private Document blobDocument;

    @BeforeClass
    public static void setUpClass() throws Exception {
        ChinoBaseTest.runClass(BlobsTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        chino_noauth = new ChinoAPI(TestConstants.HOST);  // to test the direct BLOB link
        test = ChinoBaseTest.init(chino_admin.blobs);

        // Create a repository and a Schema for the Blob's Document
        boolean isClean = true;
        for (Repository r : chino_admin.repositories.list().getRepositories()) {
            if (r.getDescription().contains("BlobsTest")) {
                isClean = false;
            }
        }
        ChinoBaseTest.checkResourceIsEmpty(
                isClean,
                chino_admin.blobs
        );

        REPO_ID = chino_admin.repositories.create("BlobsTest"  + " [" + TestConstants.JAVA + "]").getRepositoryId();

        LinkedList<Field> fields = new LinkedList<>();
        fields.add(new Field("testName", "string"));
        fields.add(new Field(blobFieldName, "blob"));
        SchemaStructure s = new SchemaStructure(fields);
        SCHEMA_ID = chino_admin.schemas.create(REPO_ID,
                "This schema is used for Documents that store BLOBs in BlobsTest", s).getSchemaId();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        // cleanup leftovers
        if(blobId != null)
            try {
                chino_admin.blobs.delete(blobId);
                blobId = null;
            } catch (IOException | ChinoApiException e) {
                System.err.println("ERROR! Blob not deleted: " + blobId);
            }

        for (String fileName : outputFiles) {
            File outputFile = new File(fileName);
            if (outputFile.exists())
                if (outputFile.delete()) {
                    System.out.println("Deleted: " + outputFile.getPath());
                }
        }

        ChinoBaseTest.afterClass();
    }

    @Before
    public void createBlobDocument() throws IOException, ChinoApiException {
        // create new Blob's Document
        HashMap<String, String> fileContent = new HashMap<>();
        fileContent.put("testName", "BlobsTest");
        blobDocument = chino_admin.documents.create(SCHEMA_ID, fileContent);
    }

    @Test
    public void testChunkOperations() throws NoSuchAlgorithmException, ChinoApiException, IOException {
        String filename;

        // check default chunk size
        assertEquals("Wrong value for default chunk size", 1024*1024, Blobs.getDefaultChunkSize());

        // with InputStream
        filename = blobFileName.replace(".jpg", "_big.jpg");
        FileInputStream sourceStream = new FileInputStream(resFolder + filename);
        System.out.println("- Using FileInputStream (many chunks):");
        runTestUploadLinkGetDelete(sourceStream, filename);
        sourceStream.close();

        // with File path
        filename = blobFileName.replace(".jpg", "_big.jpg");
        System.out.println("- Using File path (many chunks):");
        runTestUploadLinkGetDelete(resFolder, filename);
    }

    @Test
    public void testByFilepath_upload_get_delete() throws NoSuchAlgorithmException, ChinoApiException, IOException {
        String sourceFolder = resFolder;
        String filename = blobFileName;
        System.out.println("- Using File path:");
        runTestUploadLinkGetDelete(sourceFolder, filename);
    }

    @Test
    public void testByFileInputStream_upload_get_delete() throws NoSuchAlgorithmException, ChinoApiException, IOException {
        FileInputStream sourceStream = new FileInputStream(resFolder + blobFileName);
        String filename = blobFileName.replace(".jpg", "_fileIS.jpg");
        System.out.println("- Using FileInputStream:");
        runTestUploadLinkGetDelete(sourceStream, filename);
        sourceStream.close();
    }

    @Test
    public void testByByteInputStream_upload_get_delete() throws NoSuchAlgorithmException, ChinoApiException, IOException {
        byte[] bytes = Files.readAllBytes(Paths.get(resFolder + blobFileName)); // keep the test file size SMALL!
        ByteArrayInputStream sourceStream = new ByteArrayInputStream(bytes);
        String filename = blobFileName.replace(".jpg", "_byteIS.jpg");
        System.out.println("- Using ByteInputStream:");
        runTestUploadLinkGetDelete(sourceStream, filename);
        sourceStream.close();
    }

    private void runTestUploadLinkGetDelete(Object source, String filename)
            throws IOException, ChinoApiException, NoSuchAlgorithmException
    {
        /* UPLOAD */
        CommitBlobUploadResponse res_upload;

        if (source instanceof String) {
            res_upload = chino_admin.blobs.uploadBlob((String) source, blobDocument, blobFieldName, filename);
        } else if (source instanceof InputStream) {
            res_upload = chino_admin.blobs.uploadBlob((InputStream) source, blobDocument, blobFieldName, filename);
        } else {
            String msg = "Unrecognized BLOB source: %s.\n" +
                    "You can only upload from an InputStream or specifying the path of the source file in a String.";
            fail(String.format(msg, source.getClass().getCanonicalName()));
            return; // it's here to prevent compilation errors
        }

        long expectedSize = res_upload.getBlob().getBytes();

        blobId = res_upload.getBlob().getBlobId();

        System.out.println("BLOB uploaded successfully! ID: " + blobId);

        /* GENERATE UNIQUE LINK */
        // This is not a one-time link (it's in another test) and 5 minutes validity is long enough.
        String blobLink = test.generateLink(blobId, 5);

        /* GET */
        long size;
        String responseFileName;

        if (source instanceof String) {
            // TEST 1: read BLOB directly to file
            // 1.1 -> use normal call; read file name from "Content-Disposition" header
            GetBlobResponse blob = chino_admin.blobs.get(blobId, outputPath);
            size = blob.getSize();
            responseFileName = blob.getFilename();

            // 1.2 -> use direct link from a non-authenticated client
            GetBlobResponse blobFromLink = chino_noauth.blobs.getFromLink(blobLink, linkOutputPath, filename);
            // compare BLOB read with the direct URL with the other one
            assertEquals("BLOB read vs link: size mismatch", size, blobFromLink.getSize());
            assertEquals("BLOB read vs link: file name mismatch", responseFileName, blobFromLink.getFilename());
            assertEquals("BLOB read vs link: MD5 mismatch", blob.getMd5(), blobFromLink.getMd5());
            assertEquals("BLOB read vs link: SHA1 mismatch", blob.getSha1(), blobFromLink.getSha1());

            // store the path to downloaded files, so they can be deleted at the end of tests
            outputFiles.add(outputPath + File.separator + responseFileName);
            outputFiles.add(linkOutputPath + File.separator + blobFromLink.getFilename());
        } else /* if (source instanceof InputStream) */ {
            // TEST 2: read BLOB as InputStream
            // 2.1 -> use normal call
            responseFileName = filename;
            Path outputFile = Paths.get(outputPath + File.separator + filename);
            if (!Files.exists(outputFile)) { Files.createDirectories(outputFile); }
            try (InputStream blob = chino_admin.blobs.getByteStream(blobId)) {
                System.out.println(outputFile.toAbsolutePath());
                Files.copy(blob, outputFile, StandardCopyOption.REPLACE_EXISTING);
            }
            size = Files.size(outputFile);

            // 2.2 -> use direct link from a non-authenticated client
            Path outputFileFromLink = Paths.get(linkOutputPath + File.separator + filename);
            if (!Files.exists(outputFileFromLink)) { Files.createDirectories(outputFileFromLink); }
            try (InputStream blobFromLink = chino_noauth.blobs.getByteStreamFromLink(blobLink)) {
                System.out.println(outputFileFromLink.toAbsolutePath());
                Files.copy(blobFromLink, outputFileFromLink, StandardCopyOption.REPLACE_EXISTING);
            }

            outputFiles.add(outputFileFromLink.toString());
        }

        // Common tests: compare the size of the downloaded BLOB to the original file size
        assertTrue("Created an empty blob!", expectedSize > 0);
        assertTrue("Read an empty blob!", size > 0);
        assertEquals(expectedSize, size);

        System.out.println("BLOB read successfully! size: " + size + ", filename: " + responseFileName);

        /* DELETE */
        chino_admin.blobs.delete(blobId);

        try {
            chino_admin.blobs.get(blobId, outputPath, filename);
            fail("Blob was not deleted!");
        } catch (ChinoApiException e) {
            if (! e.getCode().equals("404")) {
                // there is some other issues with the API call
                throw e;
            }
            blobId = null;
            System.out.println("BLOB deleted successfully!");
            System.out.println();
        }
    }

    @Test
    public void testByFilePath_get_exception() {
        // test mapping of error API response to ErrorResponse class
        System.out.println("- Using empty BLOB ID");
        // Refer to AOB-123
        Assume.assumeTrue("Running tests against '" + TestConstants.HOST + "'. " +
                        "This test can only be run on Sandbox/Production environment on api[.test].chino.io.",
                TestConstants.HOST.contains("api.test.chino.io") || TestConstants.HOST.contains("api.chino.io"));
        try {
            chino_admin.blobs.get("", outputPath);
            fail("Read blob without ID?!");
        } catch (JsonMappingException e) {
            fail("error in mapping object to ErrorResponse" + e.getMessage());
        } catch (Exception e) {
            assertTrue(e.getMessage(), e instanceof ChinoApiException);
            assertTrue(e.getMessage(), e.getMessage().contains("404"));
            System.out.println("Successfully thrown exception: " + e.getMessage());
            System.out.println();
        }
    }

    @Test
    public void testByInputStream_get_exception() {
        // test mapping of error API response to ErrorResponse class
        System.out.println("- Using empty BLOB ID");
        // Refer to AOB-123
        Assume.assumeTrue("Running tests against '" + TestConstants.HOST + "'. " +
                        "This test can only be run on Sandbox/Production environment on api[.test].chino.io.",
                TestConstants.HOST.contains("api.test.chino.io") || TestConstants.HOST.contains("api.chino.io"));
        try {
            chino_admin.blobs.getByteStream("");
            fail("Read blob without ID?!");
        } catch (JsonMappingException e) {
            fail("error in mapping object to ErrorResponse" + e.getMessage());
        } catch (Exception e) {
            assertTrue(e.getMessage(), e instanceof ChinoApiException);
            assertTrue(e.getMessage(), e.getMessage().contains("404"));
            System.out.println("Successfully thrown exception: " + e.getMessage());
            System.out.println();
        }
    }

    @Test
    public void testBlobOneTimeLinkAndMetadata() throws IOException, ChinoApiException {
        String sourceFolder = resFolder;
        String filename = blobFileName;
        System.out.println("- Using File path:");
        CommitBlobUploadResponse resp = chino_admin.blobs.uploadBlob(sourceFolder, blobDocument, blobFieldName, blobFileName);
        String blobId = resp.getBlob().getBlobId();

        // Generate one-time-only link from the Metadata
        BlobURLMetadata metadata = test.generateLinkMetadata(blobId, 5, true);
        assertNotNull(metadata.getToken());
        assertNotNull(metadata.getExpiration());
        assertTrue(metadata.isOneTime());

        String oneTimeLink = metadata.getFullBlobLink(TestConstants.HOST, blobId);

        // First read (expect success)
        InputStream result = chino_noauth.blobs.getByteStreamFromLink(oneTimeLink, "image/jpeg");
        // trying to read() the Stream to a byte array should not return a '-1'.
        assertNotEquals("Received an empty byte stream from the BLOB link",
                -1, result.read(new byte[1]));

        // Second read (expect failure)
        try {
            chino_noauth.blobs.getByteStreamFromLink(oneTimeLink);
            fail("One-time link is available to read for the 2nd time");
        } catch (ChinoApiException e) {
            assertEquals("Unexpected status code on reading BLOB from one-time link",
                    "404", e.getCode());
        }

        // Test metadata
    }

    @After
    public void tearDown() throws Exception {
        // delete Document
        if (blobDocument != null) {
            chino_admin.documents.delete(blobDocument.getDocumentId(), true);
            blobDocument = null;
        }

        File outputFile = new File(outputPath);
        if (outputFile.exists())
            outputFile.delete();
    }
}