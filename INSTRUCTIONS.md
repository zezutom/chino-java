#  Full instructions
 
Useful links:
 - [Chino.io API docs](https://docs.test.chino.io/custodia/docs/v1)
 - [javadoc](https://chinoio-public.gitlab.io/chino-java)
 - [javadoc (up to v. 3.3.2)](https://jitpack.io/com/github/chinoio/chino-java/3.3.2/javadoc/io/chino/java/package-summary.html)

For issues or questions, please contact
[tech-support@chino.io](mailto:tech-support@chino.io).

------------------------------------------------------------------------
***NOTE:***

Because of the new policies of Oracle about Java updates, we strongly
suggest to adopt one of the open source builds of Java, which are
provided by [AdoptOpenJDK](https://adoptopenjdk.net/about.html):

- [Java 8 with OpenJ9](https://adoptopenjdk.net/installation.html?variant=openjdk8&jvmVariant=openj9#)
- [Java 11 with OpenJ9](https://adoptopenjdk.net/installation.html?variant=openjdk11&jvmVariant=openj9#)

Other free Java distributions can be found, provided by Azul, IBM, Red
Hat, Linux distros and more.

------------------------------------------------------------------------

### Table of Contents

- [Full instructions](#full-instructions)
    - [Table of Contents](#table-of-contents)
  - [Setup](#setup)
    - [Build with Maven](#build-with-maven)
    - [Build with Gradle](#build-with-gradle)
    - [Build from source](#build-from-source)
  - [Javadoc](#javadoc)
    - [Generate the Javadoc](#generate-the-javadoc)
  - [Usage](#usage)
  - [SDK overview](#sdk-overview)
    - [The ChinoAPI client](#the-chinoapi-client)
    - [Users `io.chino.java.Users` and UserSchemas `io.chino.java.UserSchemas`](#users-iochinojavausers-and-userschemas-iochinojavauserschemas)
      - [NOTE](#note)
    - [Auth `io.chino.java.Auth`](#auth-iochinojavaauth)
    - [Applications `io.chino.java.Applications`](#applications-iochinojavaapplications)
    - [Groups `io.chino.java.Groups`](#groups-iochinojavagroups)
    - [Permissions `io.chino.java.Permissions`](#permissions-iochinojavapermissions)
    - [Repositories `io.chino.java.Repositories`](#repositories-iochinojavarepositories)
    - [Schemas `io.chino.java.Schemas`](#schemas-iochinojavaschemas)
      - [NOTE](#note-1)
    - [Documents `io.chino.java.Documents`](#documents-iochinojavadocuments)
    - [BLOBs `io.chino.java.Blobs`](#blobs-iochinojavablobs)
    - [Search `io.chino.java.Search`](#search-iochinojavasearch)
    - [Collections `io.chino.java.Collections`](#collections-iochinojavacollections)
  - [GitLab pipelines](#gitlab-pipelines)
  - [Testing](#testing)
  - [Support](#support)
  
------------------------------------------------------------------------

## Setup

Up to version `3.3.2` we used to deliver the SDK code using Jitpack.
Refer to https://jitpack.io/#chinoio/chino-java if you want to install 
an older version of the SDK.

Though, we suggest to stick with the latest build, which contains the 
latest and greatest API updates and can be obtained from the 
**Maven Central Repository**.

### Build with Maven
Edit your project's `pom.xml` and add the `chino-java` dependency.

> SDK version up to `3.3.2`:

```xml
<repositories>
    <!-- other repositories... -->
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>

<dependency>
    <!-- other dependencies... -->
    <groupId>com.github.chinoio</groupId>
        <artifactId>chino-java</artifactId>
    <version>VERSION</version>
</dependency>
```

> SDK version from `3.4.0`:

```xml
<dependency>
    <!-- other dependencies... -->
    <groupId>io.chino</groupId>
    <artifactId>chino-java</artifactId>
    <version>VERSION</version>
</dependency>
```

### Build with Gradle
Edit your project's `build.gradle` and add the `chino-java` dependency.

> SDK version up to `3.3.2`:

```groovy
allprojects {
    repositories {
        // other repositories...
        maven { url 'https://jitpack.io' }
    }
}

dependencies {
    // other dependencies...
    compile 'com.github.chinoio:chino-java:VERSION'
}
```

> SDK version from `3.4.0`:
```groovy
dependencies {
    // other dependencies...
    compile 'io.chino:chino-java:VERSION'
}
```

#### NOTE:  <!-- omit in toc -->
Due to a bug in Gradle, if you're developing in **Android** 
you may also have to add the following code to `build.gradle`

```groovy
android {
    // other settings...
    packagingOptions {
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/LICENSE'
    }
}
```

### Build from source

1. Clone the repository locally and *cd* into the root folder:

        git clone https://gitlab.com/chinoio-public/chino-java.git
        cd chino-java

2. Build the SDK with the command
    
        ./gradlew jar

    The output will be in the `/build/libs` folder.

3. if you want a "fat jar" with all the dependencies inside, run

        ./gradlew build shadowJar

4. Finally, you can generate the Javadoc for this project.
   Continue reading the next section to learn how.

## Javadoc
You can find a **live version** of the Javadoc deployed with GitLab
pages at https://chinoio-public.gitlab.io/chino-java.

This version is updated every time something is pushed on the `master`
branch. You can also [generate it manually](#generate-the-javadoc) and
distribute as HTML or JAR.

> up to version `3.3.2`:

the Javadoc was deployed at the URL:

    https://jitpack.io/com/github/chinoio/chino-java/VERSION/javadoc/io/chino/java/package-summary.html

(*see
[javadoc for 3.3.2](https://jitpack.io/com/github/chinoio/chino-java/3.3.2/javadoc/io/chino/java/package-summary.html)*)


### Generate the Javadoc

The Javadoc for the SDK can be generated from source code:

* in **HTML** with the Gradle task `javadoc`. The files will be
  generated in `build/docs/javadoc`:

        ./gradlew build javadoc
        
* As a **JAR** with the task `javadocJar`. The JAR will be generated in
  `build/libs/:chino-java-VERSION-javadoc.jar`:
        
        ./gradlew build javadocJar

## Usage
In order to use this SDK you need to register an account at the
[Chino.io console](https://console.test.chino.io/).

From the Console you can see your API keys and create/manage resources
of Chino.io, such as Users and Documents.

### "Hello, World!" Document <!-- omit in toc -->
The most common task you can perform is the creation of a Document,
where data can be stored.

1. Create a ChinoAPI variable with your `customer_id` and `customer_key`
    ```Java
    String customerId, // paste your Customer ID
       customerKey;    // paste your Customer Key
    ChinoAPI chino = new ChinoAPI("https://api.test.chino.io", customerId, customerKey);
    ```

2. Create a Repository
    ```Java
    Repository r = chino.repositories.create("Sample repository");
    ```

3. Create a Schema inside the Repository.
    ```Java
    List<Field> fieldsList = new LinkedList<>();
    fieldsList.add(new Field ("the_content", "string"));
    SchemaStructure schemaFields = new SchemaStructure(fieldsList);
    Schema s = chino.schemas.create(r.getRepositoryId(), "Sample Schema", schemaFields);
    ```

4. Create a Document
    ```Java
    HashMap<String, Object> values = new HashMap<>();
    values.put("the_content", "Hello, World!");
    Document d = chino.documents.create(s.getSchemaId(), values);
    ```

And that's it! From here you can retrieve, update or delete your
Document with a single API call.

## SDK overview
The Java SDK implements all the features that are offered by **Chino.io
API**.

The package `io.chino.java` contains everything you need to work with
this SDK; we provide an overview of how it can be used.

*For more detail about our API, refer to the
[full Chino.io docs](https://docs.test.chino.io/custodia/docs/v1)*
 
### The ChinoAPI client
***Note:*** Check out
[ChinoAPITest.java](https://github.com/chinoio/chino-java/blob/develop/src/test/java/io/chino/java/ChinoAPITest.java)
to see some practical usage examples.

You will use this class to perform every call to Chino.io API:
```Java
ChinoAPI chino = new ChinoAPI(<host_url>);
```
Use the `host_url` of the test API during development:
**https://api.test.chino.io**

However, with this client most of the operations won't be available: you
need to authenticate first. There are two authentication options:

* Authenticate with **customer credentials** (found on Chino.io
  console). Only for admin access -
  [Learn more](https://docs.test.chino.io/custodia/docs/v1#header-application-developers)
    ```Java
    ChinoAPI chino = new ChinoAPI(<host_url>, <customer_id>, <customer_key>);
    ```
    
* Authenticate with **bearer token** - get one using class
  [Auth](#auth-iochinojavaauth) - then:
    ```Java
    ChinoAPI chino = new ChinoAPI(<host_url>, <bearer_token>);
    ```

You can change the authentication type anytime with:
```Java
chino.setCustomer(<customer_id>, <customer_key>);
chino.setBearerToken(<bearer_token>);
```
    
### Users `io.chino.java.Users` and UserSchemas `io.chino.java.UserSchemas`
API clients for managing UserSchemas and Users.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#applications)

A **User** on Chino.io represents a person and is bound to a **username
& password** pair, which can be used to log in to
[Applications](#applications-iochinojavaapplications) and get access
tokens. In the User object can be stored additional attributes, the name
and type of which is defined in a **UserSchema**.

Other usage of Users include differentiating between roles (doctor and
patients), identity verification, setting different access
[Permissions](#permissions-iochinojavapermissions) over different
resources.

To learn more about Users, check out the
[tutorial](https://docs.test.chino.io/custodia/docs/tutorials/tutorial-users).

**"consistent" creation calls**

You can wait for indexing of Users to end before proceeding. This is
useful if you plan to [Search](#search-iochinojavasearch) for Users
right after the creation.

Use the following methods:
- `create(<user_schema_id>, <attributes>, <consistent>)`
- `update(<user_id>, <attributes>, <consistent>)`

#### NOTE
You can automatically create a UserSchema that matches the fields of a Java class.
The Java types are mapped to UserSchema field types according to this conversion table:

| Chino.io Schema Field type | Java type(s)                                                                              |
|----------------------------|-------------------------------------------------------------------------------------------|
| `integer`                  | `int`, `java.lang.Integer`                                                                |
| `array[integer]`           | `int[]`, `java.lang.Integer[]`                                                            |
| `float`                    | `float`, `java.lang.Float`                                                                |
| `array[float]`             | `float[]`, `java.lang.Float[]`                                                            |
| `string`                   | `java.lang.String`                                                                        |
| `array[string]`            | `java.lang.String[]`                                                                      |
| `text`                     | n.a.                                                                                      |
| `boolean`                  | `boolean`, `java.lang.Boolean`                                                            |
| `date`                     | `java.util.Date`, `java.sql.Date`                                                         |
| `time`                     | `java.sql.Time`                                                                           |
| `datetime`                 | `java.sql.Timestamp`, `java.security.Timestamp`                                           |
| `base64`                   | `java.util.Base64`                                                                        |
| `json`                     | `java.util.HashMap`, `java.util.LinkedHashMap`, `com.fasterxml.jackson.databind.JsonNode` |

- **NEW (v 4.1.0): Datatype conversion**

    Datatype Conversion is an **async** operation that can be triggered on the fields of a UserSchema and
    **may cause loss of data**.
    Be sure to [read the documentation](https://docs.chino.io/custodia/docs/v1#user-schemas-userschema-datatype-conversion)
    nd understand the possible consequences before you use this feature.

    ```java
    // Create a Schema
    List<Field> fields = new LinkedList<>();
    Field strField = new Field ("example_str", "string");
    fields.add(strField);
    Field intField = new Field ("example_int", "integer");
    fields.add(intField);
    
    UserSchemaStructure struc = new UserSchemaStructure(fields);
    UserSchemaRequest req = new UserSchemaRequest("UserSchema created for SchemaTest in Java SDK", struc);
    String userSchemaId = chino_admin.userSchemas.create(req).getUserSchemaId();
    
    // change the datatype of the integer field to string
    intField.setType("string");
    intField.setDefault("NOT SET");
    
    // start datatype conversion, passing all the Fields as varargs
    DatatypeConversionStarted outcome = test.startDatatypeConversion(userSchemaId, strField, intField);
    
    // Check progress of Datatype Conversion operation
    DatatypeConversionStatus conversionProgress = test.getDatatypeConversionStatus(userSchemaId);
    do {
        ConversionStatus status = conversionProgress.getStatus();
        if (status == ConversionStatus.COMPLETED) {
            System.out.println("Conversion completed.")
        } else if (status == ConversionStatus.FAILED_CONVERSION || status == ConversionStatus.FAILED_REINDEX) {
            throw new Exception("Failed Datatype Conversion");
        }
    } while (conversionProgress.getStatus() != ConversionStatus.COMPLETED);
    ```

### Auth `io.chino.java.Auth`
API client for User authentication.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#user-authentication)

Auth works together with
[Applications](#applications-iochinojavaapplications) to provide OAuth2
authentication to Users.
[Read more about Chino.io and OAuth2](https://docs.test.chino.io/custodia/docs/tutorials/tutorial-auth)

When working with multiple Users, start with a non-authenticated client
```Java
ChinoAPI chino = new ChinoAPI(<host_url>);
```

Then ask the User to input its username and password and login with
those credentials. Users login to an Application, so you should create
one and save the `applicationId` and `applicationSecret`.

```Java
LoggedUser tokens = chino.auth.login(username, password, appId, appSecret);
```

The `LoggedUser` object contains an **access_token** (a.k.a. bearer
token) and a **refresh token** that are also stored in the `chino`
client after the method returns and used to authenticate the API calls.

### Applications `io.chino.java.Applications`
API client for User authentication and management of OAuth2 clients.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#applications)

Applications on Chino.io implement the "client credentials grant" of
OAuth2.
[Read more about Chino.io and OAuth2](https://docs.test.chino.io/custodia/docs/tutorials/tutorial-auth)

Applications can be of two types:
1. **Public clients** that are executed on a device (such as a PC, Mac,
    smartphone, Raspberry, etc...) or run partially on the front-end
    (e.g. a script in a web browser). Those clients are considered unsafe
    and developers must *never* store credentials on them, such as the
    `customer_id` and `customer_key` of Chino.io.
    
    This type of client is represented by "public" Applications on
    Chino.io and are identified with an `application_id`.
    
2. **Confidential clients** are executed exclusively on server-side, 
    where only an admin has access. Those clients are considered a safe
    place for sensitive information.
    
    They are represented by "confidential" Applications on Chino.io and
    are identified by both an `application_id` and an
    `application_secret`.

### Groups `io.chino.java.Groups`
API client for creating and managing Groups.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#groups)

Groups can be used to collect Users regardless of their UserSchema, can
have attributes and can be granted
[Permissions](#permissions-iochinojavapermissions) over other resources.

### Permissions `io.chino.java.Permissions`
API client to manage access Permissions of Users to the resources.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#permissions)

**- new Permissions interface**:

The new system provides: 

* a `PermissionSetter` to specify which Permissions will be granted, as
  in this JSON object:
```JSON
{
  "manage" : [ /* list of permissions for the user */ ],
  "authorize" : [ /* list of permissions that the user can give to other users */ ],
  "created_document" : {
    // can only be specified for documents in a Schema;
    // default permissions that are given on new documents created in the Schema.
    "manage" : [ /* default permissions for the user */ ],
    "authorize" : [ /* default permissions that the user can give to other users */ ]
  }
}
```

* a `PermissionsRequestBuilder` to specify the target resource and the
  user which will obtain (or lose) the Permissions.
    1. specify a target resource:
        * `on(ResourceType, "resourceId")` apply Permissions to a single resource
     *    `onChildrenOf(ResourceType, "resourceId")` apply Permissions
          to every child resource. Only works with REPOSITORY, SCHEMA,
          USER_SCHEMA.
  *       `onEvery(ResourceType)` apply permissions to all resources.
          Only works with REPOSITORY, USER_SCHEMA, GROUP
    2. specify a subject:
       *  `toUser("userId")` or `to(User)`: apply Permissions to a
          single
          [User](#users-iochinojavausers-and-userschemas-iochinojavauserschemas)
       *  `toGroup("groupId")` or `to(Group)`: apply Permissions to all
          Users in a [Group](#groups-iochinojavagroups)
    3. use `buildRequest()` to get a `PermissionsRequest` and execute
       it:
        * `chino.permissions.executeRequest(PermissionsRequest)`

* some constant values (as `enum`s) that represent resources and
  permission types:
  *   `Permissions.Type`: the types of grant that can be specified, e.g.
      `CREATE`, `DELETE`, `LIST` etc...
  *   `Permissions.ResourceType`: the resources of Chino.io, e.g.
      `DOCUMENTS`, `SCHEMAS`, `GROUPS`, etc...
    
#### Example 1: Grant CRUD permissions over a Document <!-- omit in toc -->
```Java
import static io.chino.java.Permissions.Type.*;

public class GrantCRUD {
    public static void main (String[] s) {
        Document doc = chino.documents.read("some-document-id");
        PermissionsRequest req = chino.permissions.grant()
                .toUser("some-user-id")
                .on(Permissions.ResourceType.DOCUMENT, doc.getDocumentId)
                .permissions(
                        new PermissionSetter()
                        .manage(
                                CREATE,     // Permissions.Type.CREATE, UPDATE, etc:
                                READ,       // the Permissions.Type part is
                                UPDATE,     // omitted because of static import
                                DELETE      // on top of the page
                        )
                ).buildRequest();
        
        // Then use one of the following methods (they are equivalent) to execute the request:
        req.execute();
        chino.permissions.executeRequest(req);
    }
}
```

#### Example 2: Revoke permission on all Repositories <!-- omit in toc -->
In this example, User 'oldAdmin' has Permission to `LIST` every 
Repository; it also can authorize other people to do the same.

We can revoke those Permissions this way:
```Java
import static io.chino.java.Permissions.Type.*;
import static io.chino.java.Permissions.ResourceType.*;

public class RevokeAuthorization {
    public static void main (String[] s) {
        User oldAdmin = chino.users.read("oldAdmin-user-id");
        
        chino.permissions.revoke()
                    .onEvery(REPOSITORY)
                    .to(oldAdmin)
                    .permissions(
                            new PermissionSetter()
                            .manage(LIST, READ)
                            .authorize(LIST)
                    )
            .buildRequest()
        .execute();
    }
}
```

### Repositories `io.chino.java.Repositories`
API client for management of Repositories.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#repositories)

Repositories act as containers for
[Schemas](#schemas-iochinojavaschemas).
 

### Schemas `io.chino.java.Schemas`
API client for management of Schemas.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#schemas)

Schemas define the structure of
[Documents](#documents-iochinojavadocuments), i.e. the name and type of
their attributes and which ones are indexed for
[Search](#search-iochinojavasearch).

- assign default values to Schema Fields:

    ```java
    // "string" field with default value
    Field f1 = new Field("field1", "string").withDefault("the-default-string");
    // "array[float] field, indexed, with default value
    Field f2 = new Field("field2", "array[float]", true).withDefault(1.0, 2.0, 3.0);
    // "array[float]" field with empty array as the default value
    Field f3 = new Field("field3", "array[float]").withDefault();
    ```
  
- **NEW (v 4.1.0): Datatype conversion**

    Datatype Conversion is an **async** operation that can be triggered on the fields of a Schema and
    **may cause loss of data**.
    Be sure to [read the documentation](https://docs.chino.io/custodia/docs/v1#schemas-schema-datatype-conversion)
    nd understand the possible consequences before you use this feature.

    ```java
    // Create a Schema
    List<Field> fields = new LinkedList<>();
    Field strField = new Field ("example_str", "string");
    fields.add(strField);
    Field intField = new Field ("example_int", "integer");
    fields.add(intField);
    
    SchemaStructure struc = new SchemaStructure(fields);
    SchemaRequest req = new SchemaRequest("Datatype Conversion example", struc);
    String schemaId = chino_admin.schemas.create(REPO_ID, req).getSchemaId();
    
    // change the datatype of the integer field to string
    intField.setType("string");
    intField.setDefault("NOT SET");
    
    // start datatype conversion, passing all the Fields as varargs
    DatatypeConversionStarted outcome = test.startDatatypeConversion(schemaId, strField, intField);
    
    // Check progress of Datatype Conversion operation
    DatatypeConversionStatus conversionProgress = test.getDatatypeConversionStatus(schemaId);
    do {
        ConversionStatus status = conversionProgress.getStatus();
        if (status == ConversionStatus.COMPLETED) {
            System.out.println("Conversion completed.")
        } else 
    } while (conversionProgress.getStatus() != ConversionStatus.COMPLETED);
    ```

- **NEW (v 4.1.0): Dump Schema**

    This feature allows to create a dump of selected entries from a Schema. The dump is created in an async fashion and,
    once completed, can be downloaded locally.
    
    Supported file formats for the dump are JSON and CSV.

```java
// The Schema to dump
String schemaId = "a0e3041e-eb25-4239-9947-2a192e8d99fd";
// Select the Schema fields to include in the dump
LinkedList<String> fieldNames = new LinkedList<>();
fieldNames.add("name");
// Use the same syntax from the Search to include only selected records in the dump
SearchQueryBuilder query = SearchQueryBuilder.not("name", FilterOperator.EQUALS, "Paul")
// Specify one or more SortRules to sort the results in the dump file
LinkedList<SortRule> sort = new LinkedList<>();
sort.add(new SortRule("name", SortRule.Order.ASC))

// Start the async task
String dumpId = test.startSchemaDump(schemaId, fieldNames, query, sort, DumpFormat.CSV);

// Check the progress of the task
DumpStatus status = null;
do {
    SchemaDumpStatus dumpProgress = test.checkSchemaDumpStatus(dumpId);
    status = dumpProgress.getStatus();
    if (status == DumpStatus.FAILED_CONVERSION || status == DumpStatus.FAILED_REINDEX) {
        throw new Exception("Failed Schema Dump");
    }
    Thread.sleep(1000);
} while (status != DumpStatus.COMPLETED);

```
    

#### NOTE
You can automatically create a Schema that matches the fields of a Java class.
The Java types are mapped to Schema field types according to this conversion table:

| Chino.io Schema Field type | Java type(s)                                                                              |
|----------------------------|-------------------------------------------------------------------------------------------|
| `integer`                  | `int`, `java.lang.Integer`                                                                |
| `array[integer]`           | `int[]`, `java.lang.Integer[]`                                                            |
| `float`                    | `float`, `java.lang.Float`                                                                |
| `array[float]`             | `float[]`, `java.lang.Float[]`                                                            |
| `string`                   | `java.lang.String`                                                                        |
| `array[string]`            | `java.lang.String[]`                                                                      |
| `text`                     | n.a.                                                                                      |
| `boolean`                  | `boolean`, `java.lang.Boolean`                                                            |
| `date`                     | `java.util.Date`, `java.sql.Date`                                                         |
| `time`                     | `java.sql.Time`                                                                           |
| `datetime`                 | `java.sql.Timestamp`, `java.security.Timestamp`                                           |
| `base64`                   | `java.util.Base64`                                                                        |
| `json`                     | `java.util.HashMap`, `java.util.LinkedHashMap`, `com.fasterxml.jackson.databind.JsonNode` |
| `blob`                     | `java.io.File`                                                                            |

### Documents `io.chino.java.Documents`
API client for management of Documents.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#documents)

Documents are used on Chino.io to store sensitive information. Learn
more about Documents in the
[tutorial](https://docs.test.chino.io/custodia/docs/tutorials/tutorial-docs).

**"consistent" creation & update calls**

You can wait for indexing of Documents to end before proceeding. This is
useful if you plan to [Search](#search-iochinojavasearch) for Documents
right after the creation.

Use the following methods:
- `create(<schema_id>, <content>, <consistent>)`
- `update(<document_id>, <content>, <consistent>)`

### BLOBs `io.chino.java.Blobs`
API client for binary file (BLOB) upload.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#blobs)

- `uploadBlob(<path>, <document_id>, <field>, <file_name>)` this is the main function, which handles the upload 
  of a Blob from start to end.
- `get(<blob_id>, <destination>)` Download BLOB to a file (located at `<destination>`)
- `getByteStream(<blobId>)` Get a BLOB and pass it to the Java application as an `InputStream`
- `get(<blob_id>, <destination>, <filName>)` Download BLOB to a file (located at `<destination>` and 
  named `<fileName>`)
- `generateLink(<blobId>, <duration>, <oneTimeAccess>)` Generate a unique link to access the BLOB even 
  without authentication.
- `getFromLink(<blobLink>, <destination>, <fileName>, <mimeType>)` Read a BLOB from a unique link (does NOT
   require authentication) and store it to a file (located at `<destination>`)
- `getByteStreamFromLink(String blobLink, String mimeType)` Read a BLOB from a unique link (does NOT
   require authentication) and pass it to the Java application as an `InputStream`
- `delete(<blob_id>)` delete a BLOB

These methods can be used to have more control over the process of BLOB upload (chunked)
- `initUpload(<document_id>, <field>, <file_name>)`
- `uploadChunk(<upload_id>, <chunk_data>, <offset>, <length>)`
- `commitUpload(<upload_id>)`

### Search `io.chino.java.Search`
API client to perform search operations on Chino.io resources.
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#search-api)

**- new Search interface**:

We have updated our Search API, implementing:
 
* a new method `UsersSearch.usernameExists`, that allows to easily check
  if a name is already registered in a specified UserSchema. E.g.:
```java
    boolean exists = chino.search.users(<user_schema_id>).usernameExists(<username>);
```
* a more dev-friendly interface
* support for complex queries - now supporting multiple conditional
  operators (AND, OR, NOT) in one query.
* a `SearchQueryBuilder` class, that makes queries easily repeatable and
  thread-safe

The new Search requests must contain the following parameters:
* the Search domain, either a `UserSchema` or a `Schema`
* the `ResultType`, which can be one of `FULL_CONTENT`, `NO_CONTENT`,
  `ONLY_ID` and `COUNT`
* any amount (even 0) of `SortRule` objects
* a **query** describing the conditions that the search results must
  match:
  * name of an indexed **field**
  * an **operator** from class FilterOperator, i.e. one of `EQUALS`,
    `GREATER_EQUAL`, `GREATER_THAN`, `IN`, `IS`, `LIKE`,
    `LOWER_EQUAL`, `LOWER_THAN`
  * the expected **value** for the comparison

Then the query must be built using the `buildSearch()` method.

```Java
    UserSearch search = (UserSearch) chino.search
        // search domain
    .users("user-schema-id")
        // result type
    .setResultType(ResultType.FULL_CONTENT)
        // sort rules (0 or more)
    .addSortRule("first_name", SortRule.Order.ASC)
        // query starts here:
            .with("last_name", FilterOperator.EQUALS, "Rossi")
            .and("age", FilterOperator.GREATER_THAN, 59)  
        // return a search client to perform the query            
    .buildSearch();
``` 

The returned object is a subclass of `AbstractSearchClient` that can
perform that query. By calling
```java
    // return first 10 results
    search.execute();

    // return the first 50 results starting from the 5th
    int offset = 5;
    int limit = 50;
    search.execute(offset, limit);
```
you will send the API call, just like in the old search, and obtain a
GetDocumentResponse.

More complex queries can be made, e.g. nested queries.

#### Example 1 - `last_name = "Smith" OR last_name = "Snow"` <!-- omit in toc -->
simple condition (taken from the [official docs](https://docs.chino.io/))
```java
    chino.search.users(<user-schema-id>).setResultType(ResultType.FULL_CONTENT).addSortRule("first_name", SortRule.Order.ASC)
         .with("last_name", FilterOperator.EQUALS, "Smith")
         .or("last_name", FilterOperator.EQUALS, "Snow")
         .buildSearch().execute();
```

----------------------------------------------------------

#### Example 2 - `last_name="smith" AND (age>60 OR (NOT age >= 20)))` <!-- omit in toc -->
nested queries (taken from the [official docs](https://docs.chino.io/) and
improved)
```java
    chino.search.users(<user-schema-id>).setResultType(ResultType.FULL_CONTENT).addSortRule("first_name", SortRule.Order.ASC)
             .with("last_name", FilterOperator.EQUALS, "Smith")
             .and(
                     // create new query: use the following static method
                     SearchQueryBuilder.with("age", FilterOperator.GREATER_THAN, 60)
                     .or(
                             // create negated query: another static method
                             SearchQueryBuilder.not("age", FilterOperator.GREATER_EQUAL, 20)
                     )
             )
             .buildSearch().execute();
```

----------------------------------------------------------

#### Example 3 - `(NOT document_title = "empty") AND page_count <= 30` <!-- omit in toc -->
use of **static imports** to improve code readability
```java
    import static SearchQueryBuilder.with;
    import static SearchQueryBuilder.not;
    import static FilterOperator.*;
    // other imports and code...
    
    chino.search.users(<user-schema-id>).setResultType(ResultType.FULL_CONTENT).addSortRule("first_name", SortRule.Order.ASC)
             .with(
                     not("document_title", EQUALS, "empty")
             )
             .and(
                     with("page_count", LOWER_EQUAL, 30)
             )
             .buildSearch().execute();
```

#### New Search API overview: <!-- omit in toc -->

`io.chino.java.Search`:
- `users(String userSchemaId)`: get a new `UsersSearch` client.
- `documents(String schemaId)`: get a new `DocumentsSearch` client.

`io.chino.api.search.UsersSearch` & `*.DocumentsSearch` (implementations
of `*.AbstractSearchClient`):
- `setResultType(ResultType type)`: overwrite the type of the results
  returned by this search
- `addSortRule(String field, SortRule.Order order)`: add a sort rule to
  this search
- `with(String field, FilterOperator type, ? value)`: get a new
  `SearchQueryBuilder` with the search condition in the parameters.
- `with(SearchQueryBuilder query)`: get a new `SearchQueryBuilder` from
  the `query` parameter.

`io.chino.api.search.SearchQueryBuilder`:
- (static) `with(String field, FilterOperator type, ? value)`: negate
  the search condition in the parameters.
- (static) `with(SearchQueryBuilder query)`: return the `query`
  parameter.
- (static) `not(String field, FilterOperator type, ? value)`: negate the
  search condition in the parameters and return it as a new query.
- (static) `not(SearchQueryBuilder query)`: negate the `query` parameter
  and return it as a new query.
- `and(String field, FilterOperator type, ? value)`: get a new query
  that is equivalent to the original query AND the search condition in
  the parameters
- `and(SearchQueryBuilder query)`: get a new query that is equivalent to
  the original query AND the `query` parameter
- `or(String field, FilterOperator type, ? value)`: get a new query that
  is equivalent to the original query OR the search condition in the
  parameters
- `or(SearchQueryBuilder query)`: get a new query that is equivalent to
  the original query OR the `query` parameter

### Collections `io.chino.java.Collections`
API client to manage Collections of
[Documents](#documents-iochinojavadocuments).
[*See full docs*](https://docs.test.chino.io/custodia/docs/v1#collections)

## GitLab pipelines
GL Pipelines are set up for testing and for publishing the Javadoc.

For more information about the public Javadoc, check the related [chapter](#javadoc).

Only Chino.io developers are able to trigger the pipeline.

To start the automated tests:
- Copy `gitlab-dummy-properties.gradle` to `gitlab-properties.gradle`
- Fill in the projct ID and the personal token
- Run the pipeline with:

      ./gradlew gitlabPipeline

*NOTE: make sure you are using Java 8 to run the pipeline with Gradle.*

Two testing pipelines are executed, one for Java 8 and another for Java 11.

Pipelines run on any branch if triggered manually, as well as automatically on every tag.


## Testing
With the SDK are included some JUnit tests, that are used for continuous
integration. If you want (for some reason) to run these tests by
yourself, the best thing to do is to run them in an account *ad hoc*. In
fact, before and after each test session **everything on the account is
deleted**, in order to preserve the correctness of tests.

If you know what you are doing, you can launch tests with one of the
following methods:

1.  Create a file `src/test/res/test.properties` with the following
    content:
    ```properties
    chino.test.host=https://api.test.chino.io/v1
    chino.test.customer_id=<your-chino-io-customer-id>
    chino.test.customer_key=<your-chino-io-customer-key>
    chino.test.automated=allow
    ```
    Write your id/key and set `chino.test.automated=allow` to enable
    testing. You can also set `automated_test=allow` in your environment
    variables to enable testing.
    
    Then launch the test suite.
    
    **WARNING: this allows the test suite to delete everything on the
    account you use to test.**
        
        gradle test -i

    If you are running against a **production environment**, you must
    set `-Pchino.test.host.skip_prod=false`. This is done automatically
    if the host name contains `//api.chino.io`.

2.  Specify the properties of the `test` task from the command line.
        
    **WARNING: this allows the test suite to delete everything on the
    account you use to test.**
        
        gradle test -i -Pchino.test.customer_id=<your ID> -Pchino.test.customer_key=<your KEY> -Pchino.test.automated=allow

    Add ` -Pchino.test.host=<path.to.host>` in order to run tests
    against a custom host. Default test host is
    `https://api.test.chino.io/v1`.

    If you are running against a **production environment**, you must
    set `-Pchino.test.host.skip_prod=false`. This is done automatically
    if the host name contains `//api.chino.io`.
    
After every test, all the related object will be deleted. (E.g. after
running the `ApplicationsTest` test class, every existing *Application*
on the account will be lost forever.)

**NOTE: some tests are executed only against the `*.chino.io` domain.**

You can force-run those tests on other domains (e.g. ad hoc installations) 
by adding `chino.test.force_all=true` in `test.properties` or in your 
environment, but keep in mind that they may not work properly.

**NOTE - additional properties:**

When running tests, or in case you are debugging against non-production API,
you can load an additional `.properties` file that will affect the runtime behaviour.
The supported properties are:

- `chino.debug.request.print_all=[true, false: Default]`:
  before every request sent to the Chino.io API, a recap will be printed to stdout
  like the following example:
  ```text
  *** REQUEST
  GET https://api.test.chino.io/v1/repositories
  *** HEADERS
  Authorization: Basic <sanitized>
  User-Agent: okhttp/3 chino-java/4.7.0
  Connection: Keep-Alive
  Host: api.test.chino.io
  Accept-Encoding: gzip
  ```
- `chino.debug.request.hide_auth=[true: Default, false]`:
  if `print_all` is enabled, the Auth headers will be automatically sanitized.
  Setting this flag to `false` will skip this process and show all the Auth headers.

More may be added in future.

---

Tests are implemented with JUnit 4 and they cover the following classes:
* `io.chino.java.Applications`
* `io.chino.java.Auth`
* `io.chino.java.Blobs`
* `io.chino.java.ChinoAPI`
* `io.chino.java.Collections`
* `io.chino.java.Consents` [**deprecated** - will be removed in version 5.0.0]
* `io.chino.java.Documents`
* `io.chino.java.Groups`
* `io.chino.java.Permissions`
* `io.chino.java.Repositories`
* `io.chino.java.Schemas`
* `io.chino.java.Search`
* `io.chino.java.UserSchemas`
* `io.chino.java.Users`

Deprecated methods have been skipped; this can cause some classes to
appear to be covered for less than 100% in a coverage report.

## Support
Please report problems and ask for support to
[tech-support@chino.io](mailto:tech-support@chino.io).

If you want to learn more about Chino.io, visit the
[official site](https://www.chino.io) or [email us](mailto:info@chino.io).
