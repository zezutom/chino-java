# What's new
NOTE: when releasing a new version, please update the version in _build.gradle_ (`project.version`) and _README.md_.

## [4.8.0] - 2023-05-26
- Added support for the "Blocked users" API in `io.chino.api.Users`

## [4.7.1] - 2023-03-28
- Fixed bug that caused duplicated request headers
  - Prevents issues with some load balancers (Amazon EC2 and potentially others)
- Added runtime debug options to `ChinoAPI`

## [4.7.0] - 2022-08-24
- Added method to get the attributes of a *Group* as a `HashMap`.
- Added configuration for automated GitLab pipeline (exclusive to Chino.io developers)
- Added support for `all_content` flag when deleting *Schemas* and *UserSchemas*

## [4.6.2] - 2022-07-05
- Fixed bug: omit `authorize` in the Permissions JSON if the `authorize` list is empty

## [4.6.1] - 2022-06-16
- Fixed `/users/bulk_get` implementation problems with inconsistent responses

## [4.6.0] - 2022-06-13
- Support for the `/users/bulk_get` endpoint. See Users API class.

## [4.5.0] - 2022-05-23
- Support for the `/documents/bulk_get` endpoint. See Documents API class.

## [4.4.3] - 2022-04-13
- Support for the `check_reuse` flag of the Users resource

## [4.4.2] - 2021-11-24
- Support for the `uid_type` flag to receive the UUID as the `username` in the OAuth Introspect response

## [4.4.1] - 2021-10-25
- Updated version of OkHttp3 library to prevent bug on Android

## [4.4.0] - 2021-07-13
- Added support for OAuth Introspect endpoint

## [4.3.1] - 2021-07-02
- Fixed minor bug in tests for dump of UserSchemas and Schemas

## [4.3.0] - 2021-05-26
- Added support for UserSchema dump endpoint

## [4.2.0] - 2021-05-12
- Added support for filters in the Documents LIST operation

## [4.1.0] - 2021-04-15
- Added support for Schema dump endpoint
- Added support for datatype conversion in Schemas and UserSchemas

## [4.0.0] - 2021-03-23
- **Removed deprecated Search API** from `io.chino.java.Search`
- **Removed deprecated code of Permissions**
- **Removed deprecated code** from classes in `io.chino.api.common`

## [3.8.1] - 2021-03-22
- Fixed bug in parsing of `Permissions.Resourcetype`
- Skip failing tests on ad-hoc servers

## [3.8.0] - 2021-03-18
- Deprecated the old Consent Management API
  - This API is being deprecated in favor of the new [Consenta API](https://docs.chino.io/consent/consentame/docs/v1).

## [3.7.2] - 2021-01-11
- Improved serialization of Schema Fields from the fields of a class:
  - Full support for primitive type wrappers (e.g. `Float`)
  - Support for "json" type for class attributes of type `HashMap`, `LinkedHashMap` or `JsonNode`.
  - Support for "base64" type for for class attributes of type `java.util.Base64`.

## [3.7.1] - 2021-01-08
- Improved class `io.chino.api.permission.Permission`

## [3.7.0] - 2020-11-25
- Added support for `default` value in UserSchemas and Schemas fields.
  [View docs](https://docs.test.chino.io/custodia/docs/v1#header-documents-and-default-fields)

## [3.6.0] - 2020-09-29
- Support for [direct BLOB URL with Token](https://docs.test.chino.io/custodia/docs/v1#blobs-blob-url-post)
- Save BLOBs with a custom file name

## [3.5.1] - 2020-09-07
- Added method to set attributes of a _User_ using a `HashMap`

## [3.5.0] - 2020-09-04
Added methods for new endpoints:
- [Search Schemas by description](https://docs.test.chino.io/custodia/docs/v1#schemas-schemas-get)
- [Search Repositories by description](https://docs.test.chino.io/custodia/docs/v1#repositories-repositories-get)
- [Search Groups by group name](https://docs.test.chino.io/custodia/docs/v1#groups-groups-get)
- [Search UserSchemas by description](https://docs.test.chino.io/custodia/docs/v1#user-schemas-userschemas-get)
- [Search Collections by name](https://docs.test.chino.io/custodia/docs/v1#collections-search-post)
- [Filter Collections that contain a Document](https://docs.test.chino.io/custodia/docs/v1#collections-collection-object-get-1)
- [Partially update a Document](https://docs.test.chino.io/custodia/docs/v1#documents-document-object-patch)

## [3.4.3] - 2020-06-01
Fixed method to list all Consents by User ID

## [3.4.2] - 2020-03-24
Added method to list all Users in a Group

## [3.4.1] - 2020-01-28
Started tracking changes in CHANGELOG (with date and version)

Add support for insensitive fields in Schemas and UserSchemas

## [3.4.0] - 2020-01-27
Migrated SDK from GitHub to GitLab

Published javadoc on https://chinoio-public.gitlab.io/chino-java

Changed release method: now the SDK is available from Maven Central.
Check the [Setup](#setup) section for more information.

- - -
  
*Versions below are released on [jitpack.io](https://jitpack.io/) 
instead of Maven Central*


## 3.3.2
Fixed host normalization: now the client automatically removes trailing
slashes and adds `/v1`to the Chino API host URL.

The message that used to warn about using the API over HTTP has been
silenced.
    
## 3.3.1
Now it is possible to customize the User-Agent header, appending a
`<client name>`. It will look like:

    okhttp/3 chino-java/3.3.1(<client name>)

## 3.3.0
Added support to activate resources. When a resource is deactivated with
`force=false`

	delete(id, false)

Now it can be re-activated with an `update()`:

	update(true, ...)

Also, invalid delete methods for resources that can't be deactivated
(Applications, BLOBs) have been deprecated.

## 3.2.0

### Support for Java 11
The project now officially supports Java 11, which is the current LTS
version. Backwards compatibility with Java 8 is still granted, but
developers should upgrade their Java version to keep up with the
[Java SE support roadmap](https://www.oracle.com/technetwork/java/java-se-support-roadmap.html).

### Bug fixes, test and docs
Improved overall quality of tests and javadoc.

Fixed URL parsing in ChinoAPI, where sometimes a double slash at the end
of the API host URL wouldn't be removed.

Overall improvements and minor fixes.

## 3.1.0

### BLOBs API
Introduced new methods to the [BLOBs API](#blobs-iochinojavablobs). Now
it is possible to upload and download files as BLOBs using an
`InputStream` instead of storing them on file system.

Existing code, documentation and tests have been improved, too.

## 3.0

This new major version is backwards compatible. It is a starting point
for future changes to our SDK, that will follow the semantic versioning.

### Bug fixes
Fixed bugs that affected the Search.

### Force HTTPS on API host URL
Every client initialized with `http://api.chino.io` and
`http://api.test.chino.io` will be forced to use https instead.

### Test and coverage
Improved tests on the SDK. Now the code coverage and code quality
reports are published to
[Code Climate](https://codeclimate.com/github/chinoio/chino-java).

