# Chino Java SDK <!-- omit in toc -->
[![Maven Central](https://img.shields.io/maven-central/v/io.chino/chino-java.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22io.chino%22%20AND%20a:%22chino-java%22)

**Official Java SDK** for Chino.io API

**Version**: `4.8.0`

For older version, check the [full instructions](./INSTRUCTIONS.md).
 
**Useful links**
 - [Full SDK instructions](./INSTRUCTIONS.md)
 - [Chino.io API docs](https://docs.test.chino.io/custodia/docs/v1)
 - [javadoc](https://chinoio-public.gitlab.io/chino-java)
 - [javadoc (up to v. 3.3.2)](https://jitpack.io/com/github/chinoio/chino-java/3.3.2/javadoc/io/chino/java/package-summary.html)

For issues or questions, please contact
[tech-support@chino.io](mailto:tech-support@chino.io).

------------------------------------------------------------------------
#### Table of content <!-- omit in toc -->
- [Requirements](#requirements)
- [Installation](#installation)
  - [Maven](#maven)
  - [Gradle](#gradle)
  - [Build from source](#build-from-source)
- [Usage example](#usage-example)

------------------------------------------------------------------------
***NOTE:***

Because of the new policies of Oracle about Java updates, we strongly 
suggest to adopt one of the open source builds of Java, which are
provided by [AdoptOpenJDK](https://adoptopenjdk.net/about.html):

- [Java 8 with OpenJ9](https://adoptopenjdk.net/installation.html?variant=openjdk8&jvmVariant=openj9#)
- [Java 11 with OpenJ9](https://adoptopenjdk.net/installation.html?variant=openjdk11&jvmVariant=openj9#)

Other free Java distributions can be found, provided by Azul, IBM, Red
Hat, Linux distros and more.

------------------------------------------------------------------------

## Requirements

- Java 8 - [Download free for commercial use](https://adoptopenjdk.net/?variant=openjdk8&jvmVariant=openj9)

*If you are using **Maven**:*

 - the Maven command line utility `mvn` - [Download](https://maven.apache.org/download.cgi) - [Setup for Windows](https://maven.apache.org/guides/getting-started/windows-prerequisites.html)

*If you are using **Gradle**:*

In the project is included a Gradle wrapper for Linux (`gradlew`) and 
for Windows (`gradlew.bat`), that will download a Gradle distribution
and execute any Gradle command with it.

You can specify the Gradle version to use in the file
`gradle\wrapper\gradle-wrapper.properties`:

    distributionUrl=https\://services.gradle.org/distributions/gradle-<YOUR_GRADLE_VERSION>-all.zip

## Installation
The Chino.io Java SDK is available on Maven Central, making it easy to
include it in [Maven](#maven) and [Gradle](#gradle) builds.

You can also [build from source](#build-from-source) and include the JAR
file in your project.

### Maven

In your `pom.xml`, add the following dependency

```xml
<dependency>
    <groupId>io.chino</groupId>
    <artifactId>chino-java</artifactId>
    <version>VERSION</version>
</dependency>
```

### Gradle

Simply add `io.chino:chino-java` to your project dependencies.

The syntax may vary according to your Gradle version. To find out your
version, run

    gradle --version

- **Gradle `>=4`**

    > Groovy
    ```gradle
    dependencies {
        // other dependencies...
        implementation 'io.chino:chino-java:VERSION'
    }
    ```

    - - -

    > Kotlin
    ```gradle
    dependencies {
        // other dependencies...
        implementation ("io.chino:chino-java:VERSION")
    }
    ```

Due to a bug in gradle, if you're developing in android you have to add
also the following code to the "build.gradle" file
- **Gradle `3.X` and below**

    ```gradle
    dependencies {
        // other dependencies...
        compile 'io.chino:chino-java:VERSION'
    }
    ```

### Build from source

1. Clone the repository locally

        git clone https://gitlab.com/chinoio-public/chino-java.git

2. Build the SDK with the command

        ./gradlew jar

The output will be in the `/build/libs` folder.

3. if you want a "fat jar" with all the dependencies inside, run

        ./gradlew shadowJar

## Usage example
Sample code to get you up and running with Chino.io API and the Java SDK.

### Hello, World! <!-- omit in toc -->
The most common task you can perform is the creation of a Document,
where data can be stored.

1. Create a ChinoAPI variable with your `customer_id` and `customer_key`
    
    ```Java
    String customerId, // your Customer ID
       customerKey;    // your Customer Key
    ChinoAPI chino = new ChinoAPI("https://api.test.chino.io", customerId, customerKey);
    ```

2. Create a Repository

    ```Java
    Repository r = chino.repositories.create("Sample repository");
    ```

3. Create a Schema inside the Repository.

    ```Java
    List<Field> fieldsList = new LinkedList<>();
    fieldsList.add(new Field ("the_content", "string"));
    SchemaStructure schemaFields = new SchemaStructure(fieldsList);
    Schema s = chino.schemas.create(r.getRepositoryId(), "Sample Schema", schemaFields);
    ```

4. Create a Document

    ```Java
    HashMap<String, Object> values = new HashMap<>();
    values.put("the_content", "Hello, World!");
    Document d = chino.documents.create(s.getSchemaId(), values);
    ```

And that's it! From here you can retrieve, update or delete your 
Document with a single API call.
